using UnityEngine;
using UnityEngine.Events;

public class PerformanceCheck : MonoBehaviour
{
    static bool checkFps = true;

    uint frameCount;
    ushort[] m_fpsSamples = new ushort[15];
    int m_fpsSamplesIndex = 0;

    public UnityEvent onLowFPS;

    private void Start()
    {
        for (int i = 0; i < m_fpsSamples.Length; i++)
            m_fpsSamples[i] = 60;
        onLowFPS.AddListener(() =>
        {
            CameraController.Instance.SetEnabledPostProcessing(false);
        });
    }

    private void Update()
    {
        if (Time.timeSinceLevelLoad > 20 && Time.timeSinceLevelLoad < 40 && frameCount % 10 == 0)
        {
            var CurrentFPS = (ushort)(Mathf.RoundToInt(1f / Time.unscaledDeltaTime));
            m_fpsSamples[m_fpsSamplesIndex] = CurrentFPS;
            m_fpsSamplesIndex = (m_fpsSamplesIndex + 1) % m_fpsSamples.Length;

            ushort AverageFPS = 0;
            foreach (var fps in m_fpsSamples)
            {
                AverageFPS += fps;
            }
            AverageFPS = (ushort)(AverageFPS / m_fpsSamples.Length);

            if (AverageFPS < 35f && checkFps)
            {
                onLowFPS.Invoke();
                checkFps = false;
            }
        }
        frameCount++;
    }
}