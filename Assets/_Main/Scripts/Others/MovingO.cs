﻿using System.Collections;
using System.Collections.Generic;
using Arikan;
using Arikan.Tools;
using DG.Tweening;
using UnityEngine;

public class MovingO : OtherObject
{
    public Transform Target;
    public float MoveDuration = 4;
    public float WaitDuration = 2;

    Vector3 Offset;
    Coroutine coro;
    DG.Tweening.Core.TweenerCore<Vector3, Vector3, DG.Tweening.Plugins.Options.VectorOptions> x;


    protected override void Awake()
    {
        base.Awake();
        Offset = Target.position - transform.position;
    }

    // Start is called before the first frame update
    void OnEnable()
    {
        coro = StartCoroutine(MoveCoRo());
    }

    IEnumerator MoveCoRo()
    {
        yield return null;
        //Vector3 target = transform.localPosition + Offset;
        //x = transform.DOLocalMove(target, MoveDuration);
        Vector3 start = transform.position;
        Vector3 targetPose = Target.position;
        x = transform.DOMove(Target.position, MoveDuration).SetEase(Ease.InOutSine);
        yield return new WaitForSeconds(MoveDuration + WaitDuration);

        // if (Vector3.Distance(transform.position, targetPose) < 0.1f)
        {
            transform.position = targetPose;
            Target.localPosition *= -1;
            // transform.localScale = -transform.localScale;
            Offset = -Offset;
        }

        coro = StartCoroutine(MoveCoRo());
    }

    private void OnDisable()
    {
        if (x != null)
            x.Kill();
        if (coro != null)
            StopCoroutine(coro);
        GetComponent<ResetableTransform>().ResetTransform();
    }

    public override void OnHit(Ball ball)
    {
        base.OnHit(ball);
        gameObject.SetActive(false);
        ball.Rigid.velocity = Vector3.ClampMagnitude(ball.Rigid.velocity.SetY(ThrowPower), ThrowPower);
    }
}
