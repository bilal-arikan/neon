﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpringO : OtherObject
{
    public Transform Target;
    public float AngleRandomization = 0;
    public float ThrowMultiplier = 1;
    public float NoCollideMinSpeed = 0;

    Vector2 throwDirection => (Target.position - transform.position).normalized;
    

    public override void OnHit(Ball ball)
    {
        base.OnHit(ball);
        gameObject.SetActive(false);
        float randomized = UnityEngine.Random.Range(-AngleRandomization, AngleRandomization);

        //throwDirection = new Vector2(
        //    transform.lossyScale.x >= 0 ? transform.right.x : -transform.right.x,
        //    transform.lossyScale.y >= 0 ? transform.right.y : -transform.right.y).
        //        Rotate(ForceAngle + randomized).normalized;

        //throwDirection = ((Vector2)transform.right).Rotate(transform.lossyScale.x < 0 ? -ForceAngle : ForceAngle + randomized).normalized;
        ball.Rigid.velocity = throwDirection.Rotate(randomized) * ThrowPower * ThrowMultiplier;
        if (NoCollideMinSpeed > 0)
            ball.SetNoCollide(NoCollideMinSpeed);
    }
}
