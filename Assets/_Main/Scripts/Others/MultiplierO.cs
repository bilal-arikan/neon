﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplierO : OtherObject
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    public override void OnHit(Ball ball)
    { 
        base.OnHit(ball);
        gameObject.SetActive(false);
        ball.Rigid.velocity = Vector3.ClampMagnitude(ball.Rigid.velocity.SetY(ThrowPower), ThrowPower);
        //ball.Invoke(() => ball.gameObject.SetLayer("ball"), 10);


        var b = Ball.Spawn(ball.transform.position);
        b.gameObject.SetLayer("Ball");
        b.Rigid.velocity = ball.Rigid.velocity.SetX(-ball.Rigid.velocity.x);
        b = Ball.Spawn(ball.transform.position);
        b.gameObject.SetLayer("Ball");
        b.Rigid.velocity = ball.Rigid.velocity.SetX(-ball.Rigid.velocity.x);
        b = Ball.Spawn(ball.transform.position);
        b.gameObject.SetLayer("Ball");
        b.Rigid.velocity = ball.Rigid.velocity.SetX(ball.Rigid.velocity.x);
    }
}
