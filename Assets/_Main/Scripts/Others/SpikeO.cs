﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpikeO : OtherObject
{
    public float DegreeOffset = 90;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var closest = transform.Closest(Ball.Instances.Select(b => b.transform).ToArray());
        if (!closest)
            return;
        Vector2 diff = closest.position - transform.position;

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z + DegreeOffset);

    }
    public override void OnHit(Ball ball)
    {
        base.OnHit(ball);
    }
}
