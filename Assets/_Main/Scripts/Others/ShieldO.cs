﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldO : OtherObject
{
    public float ShieldedSeconds = 5;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    public override void OnHit(Ball ball)
    {
        base.OnHit(ball);
        gameObject.SetActive(false);
        if (ShieldedSeconds > 0)
            ball.SetShielded(ShieldedSeconds);
    }
}
