﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HidingO : OtherObject
{
    public float VisibleDuration = 3;
    public float HidenDuration = 3;
    public Animation HideAnimation;

    Coroutine coro;


    void OnEnable()
    {
        coro = StartCoroutine(HideCoRo(UnityEngine.Random.Range(0,VisibleDuration + HidenDuration)));
    }

    IEnumerator HideCoRo(float randomDelay)
    {
        yield return new WaitForSeconds(randomDelay);

        HideAnimation.Play("HidingAnim");
        yield return new WaitForSeconds(HideAnimation.clip.length);
        Coll.enabled = false;
        yield return new WaitForSeconds(HidenDuration);
        HideAnimation.Play("ShowingAnim");
        Coll.enabled = true;
        yield return new WaitForSeconds(HideAnimation.clip.length);
        yield return new WaitForSeconds(VisibleDuration);

        coro = StartCoroutine(HideCoRo(0));
    }

    public override void OnHit(Ball ball)
    {
        base.OnHit(ball);
        gameObject.SetActive(false);
        ball.Rigid.velocity = Vector3.ClampMagnitude(ball.Rigid.velocity.SetY(ThrowPower), ThrowPower);
    }
}
