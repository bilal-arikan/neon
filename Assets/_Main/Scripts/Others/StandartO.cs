﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandartO : OtherObject
{
    public float ThrowMultiplier = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void OnHit(Ball ball)
    {
        base.OnHit(ball);
        gameObject.SetActive(false);
        ball.Rigid.velocity = Vector3.ClampMagnitude(ball.Rigid.velocity.SetY(ThrowPower), ThrowPower * ThrowMultiplier);
    }
}
