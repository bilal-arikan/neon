﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MusicButton : MonoBehaviour
{
    Button btn;
    private void Awake()
    {
        btn = GetComponent<Button>();
        btn.onClick.AddListener(() =>
        {
            SoundController.Instance.MusicSource.enabled = !SoundController.Instance.MusicSource.enabled;
            if (SoundController.Instance.MusicSource.enabled)
                SoundController.Instance.MusicSource.Play();
            btn.GetComponent<CanvasRenderer>().SetAlpha(SoundController.Instance.MusicSource.enabled ? 1f : 0.2f);
            OnEnable();
            Debug.Log("Music: " + SoundController.Instance.MusicSource.enabled);
        });
    }

    private void OnEnable()
    {
        btn.GetComponent<CanvasRenderer>().SetAlpha(SoundController.Instance.MusicSource.enabled ? 1f : 0.2f);
    }
}
