﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SoundButton : MonoBehaviour
{
    Button btn;
    private void Awake()
    {
        btn = GetComponent<Button>();
        btn.onClick.AddListener(() =>
        {
            AudioListener.pause = !AudioListener.pause;
            btn.GetComponent<CanvasRenderer>().SetAlpha(!AudioListener.pause ? 1f : 0.2f);
            OnEnable();
            Debug.Log("Sound: " + !AudioListener.pause);
        });
    }

    private void OnEnable()
    {
        btn.GetComponent<CanvasRenderer>().SetAlpha(!AudioListener.pause ? 1f : 0.2f);
    }
}
