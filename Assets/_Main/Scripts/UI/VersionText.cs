﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.UI;

public class VersionText : MonoBehaviour
{
    public string format = "v{0}/{1}";

#if ODIN_INSPECTOR
    [Sirenix.OdinInspector.ReadOnly]
#endif
    public string vCode = "0";

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Text>().text = GetText();
    }
    void OnValidate()
    {
        if (TryGetComponent<Text>(out var t))
        {
            var newT = GetText();
            if (t.text != newT)
                t.text = newT;
        }
    }

    string GetText()
    {
#if UNITY_EDITOR
        var vCode = "0";
#if UNITY_ANDROID
        this.vCode = PlayerSettings.Android.bundleVersionCode.ToString();
        if (this.vCode != vCode)
            UnityEditor.EditorUtility.SetDirty(this);
#elif UNITY_IOS
        this.vCode = PlayerSettings.iOS.buildNumber.ToString();
        if (this.vCode != vCode)
            UnityEditor.EditorUtility.SetDirty(this);
#else
#endif
        return string.Format(format, Application.version, vCode);
#else
        return string.Format(format, Application.version, this.vCode);
#endif
    }
}
