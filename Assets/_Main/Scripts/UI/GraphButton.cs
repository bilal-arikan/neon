﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GraphButton : MonoBehaviour
{
    Button btn;
    private void Awake()
    {
        btn = GetComponent<Button>();
        btn.onClick.AddListener(() =>
        {
            CameraController.Instance.SetEnabledPostProcessing(!CameraController.Instance.IsEnabledPostProcessing());
            OnEnable();
        });
    }

    private void OnEnable()
    {
        btn.GetComponent<CanvasRenderer>().SetAlpha(CameraController.Instance.IsEnabledPostProcessing() ? 1f : 0.2f);
    }
}
