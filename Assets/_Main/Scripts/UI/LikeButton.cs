﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LikeButton : MonoBehaviour
{
    static bool VibrationEnabled = true;

    Button btn;
    private void Awake()
    {
        btn = GetComponent<Button>();
        btn.onClick.AddListener(() =>
        {
            Application.OpenURL("market://details?id=com.arikan.neon");
        });
    }
}
