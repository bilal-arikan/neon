﻿using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class MarketItemView : SerializedMonoBehaviour
{
    [SerializeField]
    MarketItem info;
    public MarketItem Info
    {
        get => info;
        set
        {
            // Fiyatı 0 olanları otomatik seçebilmek için
            if (value.Price == 0 && PlayerPrefs.GetInt(value.Id, 0) == 0)
                PlayerPrefs.SetInt(value.Id, 2);
            name = value.Id;
            PriceText.text = info.Price.ToString("N0");
            DescriptionText.text = info.Desc.ToUpper();
            info = value;
        }
    }

    public Image ItemImage;
    public Text PriceText;
    public Text DescriptionText;
    public Button SelectButton;
    public Button DeselectButton;
    public Button PurchaseButton;
    public bool IsPurchased => PlayerPrefs.GetInt(name, 0) == 1 || PlayerPrefs.GetInt(name, 0) == 2;
    public bool IsSelected => PlayerPrefs.GetInt(name, 0) == 2;

    //private void OnValidate()
    //{
    //    if (info.Id != "")
    //        name = info.Id;
    //}

    void Awake()
    {
        Info = info;
        PurchaseButton.onClick.AddListener(Buy);
        SelectButton.onClick.AddListener(Select);
    }

    public void OnEnable()
    {
        PurchaseButton.gameObject.SetActive(!IsPurchased);
        SelectButton.gameObject.SetActive(IsPurchased && !IsSelected);
        DeselectButton.gameObject.SetActive(IsPurchased && IsSelected);
        PurchaseButton.interactable = MarketController.Instance.Money >= Info.Price;
        PriceText.color = PurchaseButton.interactable ? PurchaseButton.colors.normalColor : PurchaseButton.colors.disabledColor;
    }

    void Buy()
    {
        MarketController.Instance.OnBuy(this);
        MarketController.Instance.Items.ForEach(i => i.OnEnable());
    }
    void Select()
    {
        MarketController.Instance.OnSelect(this);
        MarketController.Instance.Items.ForEach(i => i.OnEnable());
    }
}
