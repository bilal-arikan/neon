﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class VibraButton : MonoBehaviour
{
    Button btn;
    private void Awake()
    {
        btn = GetComponent<Button>();
        btn.onClick.AddListener(() =>
        {
            UIController.Instance.VibrationEnabled = !UIController.Instance.VibrationEnabled;
            OnEnable();
            Debug.Log("Vibra: " + UIController.Instance.VibrationEnabled);
        });
    }

    private void OnEnable()
    {
        btn.GetComponent<CanvasRenderer>().SetAlpha(UIController.Instance.VibrationEnabled ? 1f : 0.2f);
    }
}
