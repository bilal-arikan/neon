﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    public int Index => transform.GetSiblingIndex();// LevelController.Instance.Levels.IndexOf(this) : index;
    public bool IsUnlocked => Index <= 0 || LevelController.Instance.IsCompleted(Index - 1);

    public Text NoText;
    public Text RewardText;
    public Button SelectButton;
    public Image CompletedImage;

#if UNITY_EDITOR
    private void OnValidate()
    {
        var newText = (Index + 1).ToString();
        if (NoText.text != newText)
            NoText.text = newText;
        if (LevelController.Instance == null)
            return;
        RewardText.text = "+" + LevelController.Instance.Levels[Index].MoneyReward.ToString("N0");
    }
#endif
    private void Awake()
    {
        SelectButton.onClick.AddListener(() => GameController.Instance.Play(Index));
    }
    public void OnEnable()
    {
        name = Index.ToString();
        CompletedImage.enabled = LevelController.Instance.IsCompleted(Index);
        SelectButton.interactable = IsUnlocked;
    }

    [Button]
    void Complete() => LevelController.Instance.Complete(Index);
}
