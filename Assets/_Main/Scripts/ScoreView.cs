﻿using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ScoreView : SerializedMonoBehaviour
{
    ScoreView[] AllScores;
    static ScoreView LastlyAdded;

    public Text Order;
    public Text Name;
    public Text Score;
    public Image Background;
    public Score Value;

    Color DefBackColor;

    private void Awake()
    {
        DefBackColor = Background.color;
    }

    public void Init(Score score, bool light = true)
    {
        Value = score;
        Name.text = score.name.ToUpper();
        Score.text = score.score.ToString("N0");
        LastlyAdded = this;
        //AllScores.ForEach(s => s.Background.color = LastlyAdded == s && light ? s.Background.color : Color.black);
        //AllScores.ForEach(s => s.Order.text = (s.transform.GetSiblingIndex() + 1) + ".");
    }

    private void OnDisable()
    {
        AllScores = transform.parent.GetComponentsInChildren<ScoreView>();
    }

    public void SetCurrent(bool current)
    {
        Background.color = current ? DefBackColor : new Color(0.1f, 0.1f, 0.1f, 1f);
    }
}
