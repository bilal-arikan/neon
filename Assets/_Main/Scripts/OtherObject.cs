﻿using Arikan.Pooling;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class OtherObject : SerializedMonoBehaviour
{
    public static float ThrowPower = 10;

    public int ScoreValue = 10;
    public AudioClip HitClip;
    [NonSerialized]
    public Collider2D Coll;
    [NonSerialized]
    public SpriteRenderer Rend;

    // Start is called before the first frame update
    protected virtual void Awake()
    {
        Coll = GetComponent<Collider2D>();
        Rend = GetComponent<SpriteRenderer>();
    }

    public virtual void OnHit(Ball ball)
    {
        SoundController.Instance.Play(HitClip);
        Haptic.LightTaptic();
    }
}
