using Arikan;
using Sirenix.OdinInspector;
using System;
using UnityEngine;

namespace Arikan.Components
{
    public class AutoMoveAndRotate : SerializedMonoBehaviour
    {
        public Tuple<Vector3, Space> moveUnitsPerSecond = new Tuple<Vector3, Space>(Vector3.zero, Space.Self);
        public Tuple<Vector3, Space> rotateDegreesPerSecond = new Tuple<Vector3, Space>(Vector3.zero, Space.Self);
        public bool ignoreTimescale;
        private float m_LastRealTime;


        private void Start()
        {
            m_LastRealTime = Time.realtimeSinceStartup;
        }
        public void Update()
        {
            float deltaTime = Time.deltaTime;
            if (ignoreTimescale)
            {
                deltaTime = (Time.realtimeSinceStartup - m_LastRealTime);
                m_LastRealTime = Time.realtimeSinceStartup;
            }
            transform.Translate(moveUnitsPerSecond.Item1 * deltaTime, moveUnitsPerSecond.Item2);
            transform.Rotate(rotateDegreesPerSecond.Item1 * deltaTime, moveUnitsPerSecond.Item2);
        }
    }
}
