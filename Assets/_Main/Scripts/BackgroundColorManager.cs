﻿using System.Collections;
using System.Collections.Generic;
using Arikan;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundColorManager : SingletonBehaviour<BackgroundColorManager>
{

    public RawImage backgroundImage;
    public Color[] colorPalette;
    public float deltaColor = 0.1f;
    public float changeDuration = 1f;
    public Material bgParticleMaterial;


    Texture2D texture;

    [ShowInInspector, ReadOnly] float randomBgColorOffset;
    [ShowInInspector, ReadOnly] Color preColor1;
    [ShowInInspector, ReadOnly] Color preColor2;
    TweenerCore<float, float, FloatOptions> tweener;

    protected override void Awake()
    {
        base.Awake();
        backgroundImage.color = Color.white;
        texture = new Texture2D(1, 2);
        texture.wrapMode = TextureWrapMode.Clamp;
        texture.filterMode = FilterMode.Bilinear;

        randomBgColorOffset = Random.Range(0f, colorPalette.Length - 1f);

        SetColor(
            GetCurrentColor(0, randomBgColorOffset),
            GetCurrentColor(0, randomBgColorOffset + 1),
            0);
    }


    public void SetBG(int levelStep)
    {
        SetColor(
            GetCurrentColor(levelStep, randomBgColorOffset),
            GetCurrentColor(levelStep, randomBgColorOffset + 1),
            changeDuration);
    }


    Color GetCurrentColor(int currentBlockCount, float offset)
    {
        float colorID = ((currentBlockCount + colorPalette.Length) * deltaColor + offset) % colorPalette.Length;
        int colorI1 = (int)colorID;
        int colorI2 = (colorI1 + 1) % colorPalette.Length;
        float middleValue = colorID - colorI1;
        return Color.Lerp(colorPalette[colorI1], colorPalette[colorI2], middleValue);
    }


    void InitColor(Color color1, Color color2)
    {
        preColor1 = color1;
        preColor2 = color2;

        texture.SetPixels(new Color[] { color1, color2 });
        texture.Apply();
        backgroundImage.texture = texture;
    }
    void SetColor(Color color1, Color color2, float duration)
    {
        if (backgroundImage == null) return;

        float alpha = 0;
        Color startColor1 = preColor1;
        Color startColor2 = preColor2;

        if (tweener != null)
        {
            tweener.Kill();
        }

        tweener = DOTween.To(
        () => alpha,
        (v) =>
        {
            alpha = v;
            var nColor1 = Color.Lerp(startColor1, color1, v);
            var nColor2 = Color.Lerp(startColor2, color2, v);

            // bgParticleMaterial.color = nColor1;
            texture.SetPixels(new Color[] { nColor1, nColor2 });
            texture.Apply();
            backgroundImage.texture = texture;

            preColor1 = color1;
            preColor2 = color2;
        },
        1,
        duration)
        .SetTarget(this)
        .Play();
    }
}
