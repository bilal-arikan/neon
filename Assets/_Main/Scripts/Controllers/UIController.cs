﻿using System;
using System.Collections;
using System.Collections.Generic;
using Arikan;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;

public class UIController : SingletonBehaviour<UIController>
{
    [SerializeField]
    RectTransform MenuView;
    [SerializeField]
    RectTransform GameView;
    [SerializeField]
    RectTransform GameOverView;
    [SerializeField]
    RectTransform WonView;
    [SerializeField]
    RectTransform LostView;
    [SerializeField]
    RectTransform EnterNameView;
    [SerializeField]
    RectTransform PauseView;
    [SerializeField]
    RectTransform SponsoredView;
    [SerializeField]
    RectTransform MarketView;
    [SerializeField]
    RectTransform ChanceView;
    [SerializeField]
    RectTransform LevelsView;

    [Header("Menu View")]
    public Text GameNameText;
    public Button MenuV_GameV_Button;
    public Button MenuV_LevelsV_Button;
    public Button MenuV_MarketV_Button;
    public Button MenuV_LostV_Button;
    public Text NewVersionText;
    [Header("Game View")]
    public Text ScoreText;
    public Text ComboText;
    public Text StageText;
    public Button GameV_PauseV_Button;
    public Button GameV_MenuV_Button;
    public Slider SliderThrow;
    public Slider SliderShield;
    public Image SliderThrowFillImage;
    public Color SliderThrowAvailable = Color.green;
    public Color SliderThrowNotAvailable = Color.red;
    public GameObject InfoPanel;
    public Text LevelInfo1Text;
    public Text LevelInfo2Text;
    public Button TutorialPanel;
    [Header("Pause View")]
    public Button PauseV_GameV_Button;
    [Header("Sponsored View")]
    public Button buyNoAdsButton;

    [Header("GameOver View")]
    public VerticalLayoutGroup ScoresParent;
    public ScoreView ScoreViewPrefab;
    public Text OverScoreText;
    public Text IncreaseAmountText;
    public Text EarnedMoneyText;
    public AudioClip CounterSound;
    public GameObject GameEndedFreeMode;
    public GameObject GameEndedLevelMode;
    public Text CompletedText;
    public Text FailedText;
    public Button OverV_MenuV_Button;
    public Button ClaimX_Button;
    public Text ClaimX_Disabled_Text;
    [Header("Won View")]
    public Text WonLevelNoText;
    public Text WonScoreText;
    public Text WonRewardText;
    public Button WonV_MenuV_Button;
    public Button WonV_GameV_Button;
    public Button WonV_MarketV_Button;
    [Header("Lost View")]
    public Text LostLevelNoText;
    public Text LostScoreText;
    public Text LostRewardText;
    public Button LostV_MenuV_Button;
    public Button LostV_RestartButton;
    public Button LostV_MarketV_Button;
    [Header("EnterName View")]
    public Text YourOrder;
    public Text YourScore;
    public InputField InputName;
    public Button SaveScoreButton;
    [Header("Market View")]
    public Text MoneyText;
    public VerticalLayoutGroup ItemsView;
    public Button MarketV_MenuV_Button;
    public Button IAPMoneyButton;
    public Button AdsMoneyButton;
    public Button LikeMoneyButton;
    public Button ShrMoneyButton;
    public Text IAPMoneyText;
    public Text AdsMoneyText;
    public Text LikeMoneyText;
    public Text ShrMoneyText;
    public Button buyNoAdsMarketButton;
    [Header("Levels View")]
    public Button LevelsV_MenuV_Button;
    public LevelButton[] LevelButtons;
    [Header("PostProcess")]
    public PostProcessLayer PPL;
    public PostProcessVolume PPV;

    [Space]
    public List<Color> ComboColors;
    public bool VibrationEnabled
    {
        get => Haptic.enabled;
        set => Haptic.enabled = false;// value; //TODO
    }
    public string ScoreName
    {
        get => PlayerPrefs.GetString("ScoreName", "");
        set => PlayerPrefs.SetString("ScoreName", value);
    }
    Coroutine coro;
    float startTime;
    int lastShowedGameScore = -1;
    int earnedMoney;
    //[Button]
    //private void Add()
    //{
    //    ComboColors.Add(CColor.white);
    //    ComboColors.Add(CColor.lightsalmon);
    //    ComboColors.Add(CColor.tomato);
    //    ComboColors.Add(CColor.orangered);
    //    ComboColors.Add(CColor.red);
    //    ComboColors.Add(CColor.deeppink);
    //    ComboColors.Add(CColor.violetred);
    //    ComboColors.Add(CColor.purple);
    //    ComboColors.Add(CColor.dodgerblue);
    //    ComboColors.Add(CColor.cyan); //10
    //    ComboColors.Add(CColor.springgreen);
    //    ComboColors.Add(CColor.limegreen);
    //    ComboColors.Add(CColor.greenyellow);
    //    ComboColors.Add(CColor.yellow);
    //    ComboColors.Add(CColor.gold); //15
    //}

    void Start()
    {
        UIController.Instance.ShowMainView();

        GameNameText.gameObject.AddComponent<Button>().onClick.AddListener(() => Application.OpenURL(C.website_url.StringValue));
        InputName.onValueChanged.AddListener((v) =>
        {
            SaveScoreButton.interactable = v.Length > 0;
            ScoreName = v;
        });
        InputName.text = ScoreName;
        SaveScoreButton.onClick.AddListener(() =>
        {
            UIController.Instance.ClaimX_Button.gameObject.SetActive(true);
            HighScoreController.Instance.AddScore(new Score() { score = GameController.Instance.Score, name = InputName.text.ToUpper() });
            ShowGameOverView();
        });
        MenuV_GameV_Button.onClick.AddListener(GameController.Instance.PlayFree);
        MenuV_LevelsV_Button.onClick.AddListener(UIController.Instance.ShowLevelsView);
        MenuV_MarketV_Button.onClick.AddListener(UIController.Instance.ShowMarketView);
        MenuV_LostV_Button.onClick.AddListener(UIController.Instance.ShowGameOverView);
        GameV_PauseV_Button.onClick.AddListener(() => GameController.Instance.SetPaused(true));
        GameV_MenuV_Button.onClick.AddListener(() => UIController.Instance.ShowMainView(false));
        PauseV_GameV_Button.onClick.AddListener(() => GameController.Instance.SetPaused(false));
        OverV_MenuV_Button.onClick.AddListener(() => UIController.Instance.ShowMainView(false));
        MarketV_MenuV_Button.onClick.AddListener(() => UIController.Instance.MarketView.gameObject.SetActive(false));
        LevelsV_MenuV_Button.onClick.AddListener(UIController.Instance.ShowMainView);
        WonV_MenuV_Button.onClick.AddListener(UIController.Instance.ShowMainView);
        WonV_GameV_Button.onClick.AddListener(GameController.Instance.PlayNext);
        WonV_MarketV_Button.onClick.AddListener(UIController.Instance.ShowMarketView);
        LostV_MenuV_Button.onClick.AddListener(UIController.Instance.ShowMainView);
        LostV_RestartButton.onClick.AddListener(GameController.Instance.Restart);
        LostV_MarketV_Button.onClick.AddListener(UIController.Instance.ShowMarketView);
        ClaimX_Button.onClick.AddListener(TryClaimX);
        buyNoAdsMarketButton.onClick.AddListener(() =>
        {
            IAPSystem.Instance.BuyProduct("noads");
        });
        buyNoAdsButton.onClick.AddListener(() =>
        {
            IAPSystem.Instance.BuyProduct("noads");
        });
        E.AvailableNewVersion += (v) => NewVersionText.gameObject.SetActive(true);

        AdsSystem.Instance.RewardLoadedChanged += (ready) =>
        {
            AdsMoneyButton.interactable = ready;
            ClaimX_Button.interactable = ready;
            ClaimX_Disabled_Text.enabled = !ready;
        };
        ClaimX_Button.interactable = AdsSystem.Instance.IsRewardLoaded;
        UIController.Instance.ClaimX_Button.gameObject.SetActive(false);
        UIController.Instance.VibrationEnabled = UIController.Instance.VibrationEnabled;
    }
    public void ChangeColorTo(Color c, float duration)
    {
        if (coro != null)
            StopCoroutine(coro);
        coro = StartCoroutine(SetPanelColorCoRo(c, duration));
    }
    IEnumerator SetPanelColorCoRo(Color c, float duration)
    {
        startTime = Time.unscaledTime;
        do
        {
            float alpha = (Time.unscaledTime - startTime) / duration;
            PPV.profile.GetSetting<Vignette>().color.value = Color.Lerp(CColor.transparent, c, alpha);
            //PPVolume.profile.GetSetting<ChromaticAberration>().intensity.value = alpha;
            //Debug.Log(alpha + "/" + startTime + "/" + Time.unscaledTime);

            yield return null;
        } while (Time.unscaledTime < startTime + duration);

        PPV.profile.GetSetting<Vignette>().color.value = c;
        //PPVolume.profile.GetSetting<ChromaticAberration>().intensity.value = 1;
        //Debug.Log("Fade Completed");
    }

    public void ShowResult(bool isWon) => StartCoroutine(ShowResultCoRo(isWon));
    IEnumerator ShowResultCoRo(bool isWon)
    {
        UIController.Instance.MenuView.gameObject.SetActive(false);
        UIController.Instance.GameView.gameObject.SetActive(false);

        if (isWon)
        {
            MarketController.Instance.Money += LevelController.Instance.Levels[LevelController.Instance.CurrentLevel].MoneyReward;
            ShowWonView();
            TryShowSponsoredVideo();
        }
        else if (LevelController.Instance.CurrentLevel >= 0)
        {
            ShowLostView();
            TryShowSponsoredVideo();
        }
        else
        {
            UIController.Instance.ScoresParent.GetComponentsInChildren<ScoreView>(true).ForEach(scr => scr.SetCurrent(false));

            SaveScoreButton.interactable = InputName.text.Length > 0;

            if (HighScoreController.Instance.HighScores.Scores.Count < 10)
                EnterNameView.gameObject.SetActive(true);
            else
            {
                int score = GameController.Instance.Score;
                for (int i = 0; i < HighScoreController.Instance.HighScores.Scores.Count; i++)
                {
                    if (HighScoreController.Instance.HighScores.Scores[i].score < score)
                    {
                        EnterNameView.gameObject.SetActive(true);
                        TryShowSponsoredVideo();
                    }
                }
            }

            MarketController.Instance.Money += ScoreToMoney(GameController.Instance.Score);
            UIController.Instance.ClaimX_Button.gameObject.SetActive(true);

            if (EnterNameView.gameObject.activeInHierarchy)
            {
                YourOrder.text = (HighScoreController.Instance.GetOrder(GameController.Instance.Score) + 1) + ".";
                YourScore.text = GameController.Instance.Score.ToString("N0");
            }
            else
            {
                ShowGameOverView();
                TryShowSponsoredVideo();
            }
        }

        yield return null;
    }


    [Button]
    public void ShowStageChanged(int stage)
    {
        GameController.Instance.NewStageInstance.Text.text = "STAGE  " + stage;
        GameController.Instance.NewStageInstance.Text2.text = stage + "  /  " + GameController.Instance.ScoreLevels.Length;
        if (stage == 0)
            GameController.Instance.NewStageInstance.transform.position = CameraController.CameraMain.transform.position.SetY(-550).SetZ(0).SetX(0);
        else
            GameController.Instance.NewStageInstance.transform.position = CameraController.CameraMain.transform.position.AddY(15).SetZ(0).SetX(0);
    }
    public void HideFinishLine() => ShowFinishLine(GameController.Instance.SpawnPose.AddX(-15));
    public void ShowFinishLine(Vector3 pose)
    {
        GameController.Instance.FinishLineInstance.transform.position = pose;
        GameController.Instance.FinishLineInstance.LevelNo.text = LevelController.Instance.CurrentLevel.ToString();
    }
    public bool ShowLevelInfo(int level)
    {
        if (PlayerPrefs.GetInt("Tutorial", 0) == 0)
        {
            TutorialPanel.gameObject.SetActive(true);
            UniTask.Delay(3000).ContinueWith(() => TutorialPanel.onClick.AddListener(() =>
            {
                TutorialPanel.gameObject.SetActive(false);
                GameController.Instance.IsPlaying = true;
                PlayerPrefs.SetInt("Tutorial", 1);
            }));
            return false;
        }
        else
        {
            TutorialPanel.gameObject.SetActive(false);
            if (level < 0)
            {
                LevelInfo1Text.text = "INFINITE\nMODE";
                LevelInfo2Text.text = "><";
            }
            else
            {
                LevelInfo1Text.text = "LEVEL";
                LevelInfo2Text.text = (level + 1).ToString();
            }

            InfoPanel.gameObject.SetActive(true);
            UniTask.Delay(TimeSpan.FromSeconds(GameController.Instance.StartDelay)).ContinueWith(() => InfoPanel.gameObject.SetActive(false));
            return true;

        }
    }
    public void ShowGameView()
    {
        UIController.Instance.GameView.gameObject.SetActive(true);
        UIController.Instance.MenuView.gameObject.SetActive(false);
        UIController.Instance.PauseView.gameObject.SetActive(false);
        UIController.Instance.LevelsView.gameObject.SetActive(false);
        UIController.Instance.GameOverView.gameObject.SetActive(false);
        UIController.Instance.WonView.gameObject.SetActive(false);
        UIController.Instance.LostView.gameObject.SetActive(false);
        AdsSystem.Instance.HideBanner();
    }
    public void ShowLevelsView()
    {
        UIController.Instance.LevelsView.gameObject.SetActive(true);
        UIController.Instance.GameView.gameObject.SetActive(false);
    }
    public void ShowMainView() => ShowMainView(false);
    public void ShowMainView(bool showAd)
    {
        GameController.Instance.StopPlaying();
        UIController.Instance.MenuView.gameObject.SetActive(true);
        UIController.Instance.GameView.gameObject.SetActive(false);
        UIController.Instance.GameOverView.gameObject.SetActive(false);
        UIController.Instance.MarketView.gameObject.SetActive(false);
        UIController.Instance.PauseView.gameObject.SetActive(false);
        UIController.Instance.WonView.gameObject.SetActive(false);
        UIController.Instance.LostView.gameObject.SetActive(false);
        UIController.Instance.LevelsView.gameObject.SetActive(false);
    }
    public void ShowMarketView()
    {
        buyNoAdsMarketButton.gameObject.SetActive(AdsSystem.Instance.enabled);
        AdsMoneyButton.interactable = AdsSystem.Instance.IsBannerLoaded;
        if (!AdsSystem.Instance.IsRewardLoaded)
        {
            AdsSystem.Instance.LoadReward();
        }
        UIController.Instance.MarketView.gameObject.SetActive(true);
        AdsSystem.Instance.HideBanner();
    }
    public void ShowPausedView()
    {
        UIController.Instance.PauseView.gameObject.SetActive(true);
        UIController.Instance.GameView.gameObject.SetActive(false);
        AdsSystem.Instance.LoadBanner();
    }
    void ShowGameOverView()
    {
        UIController.Instance.GameOverView.gameObject.SetActive(true);
        UIController.Instance.EnterNameView.gameObject.SetActive(false);
        UIController.Instance.PauseView.gameObject.SetActive(false);

        if (lastShowedGameScore == GameController.Instance.PlayCount)
        {
            IncreaseAmountText.text = string.Empty;
            OverScoreText.text = string.Empty;
            EarnedMoneyText.text = string.Empty;
            ClaimX_Button.gameObject.SetActive(false);
        }
        else
        {
            earnedMoney = ScoreToMoney(GameController.Instance.Score);
            IncreaseAmountText.text = "+" + earnedMoney;
            OverScoreText.text = GameController.Instance.Score.ToString("N0");
            //StartCoroutine(LostScoreText.Counter(
            //    GameController.Instance.Score,
            //    0,
            //    3));
            StartCoroutine(EarnedMoneyText.Counter(
                MarketController.Instance.Money - earnedMoney,
                MarketController.Instance.Money,
                2f, //Mathf.Clamp((float)earnedMoney / 10, 0f, 1f),
                0.05f,
                delay: 0.5f,
                setFormat: (v) => "$ " + v.ToString("N0"),
                everyStep: (i) => SoundController.Instance.Play(CounterSound, 0.3f)));

            lastShowedGameScore = GameController.Instance.PlayCount;
        }
    }
    void ShowWonView()
    {
        UIController.Instance.WonView.gameObject.SetActive(true);
        UIController.Instance.LostView.gameObject.SetActive(false);
        UIController.Instance.PauseView.gameObject.SetActive(false);
        UIController.Instance.GameOverView.gameObject.SetActive(false);
        UIController.Instance.EnterNameView.gameObject.SetActive(false);
        UIController.Instance.MarketView.gameObject.SetActive(false);

        WonLevelNoText.text = (LevelController.Instance.CurrentLevel + 1).ToString();
        WonScoreText.text = GameController.Instance.Score.ToString();

        StartCoroutine(WonRewardText.Counter(
            0,
            LevelController.Instance.Levels[LevelController.Instance.CurrentLevel].MoneyReward,
            2,
            0.05f,
            delay: 0.5f,
            setFormat: (v) => "$ " + v.ToString("N0"),
            everyStep: (i) => SoundController.Instance.Play(CounterSound, 0.3f)));

        lastShowedGameScore = GameController.Instance.PlayCount;
        AdsSystem.Instance.LoadBanner();
    }
    void ShowLostView()
    {
        UIController.Instance.LostView.gameObject.SetActive(true);
        UIController.Instance.WonView.gameObject.SetActive(false);
        UIController.Instance.PauseView.gameObject.SetActive(false);
        UIController.Instance.GameOverView.gameObject.SetActive(false);
        UIController.Instance.EnterNameView.gameObject.SetActive(false);
        UIController.Instance.MarketView.gameObject.SetActive(false);

        LostLevelNoText.text = (LevelController.Instance.CurrentLevel + 1).ToString();
        LostScoreText.text = GameController.Instance.Score.ToString();
        LostRewardText.text = "$ 0";

        lastShowedGameScore = GameController.Instance.PlayCount;
        AdsSystem.Instance.LoadBanner();
    }

    private Tweener comboTweener;
    public void SetComboText(int value)
    {
        ComboText.gameObject.SetActive(value > 1);
        ComboText.text = "x" + value.ToString("N0");
        ComboText.color = ComboColors[Mathf.Clamp(value - 1, 0, ComboColors.Count - 1)].WithAlpha(ComboText.color.a);
        comboTweener?.Complete();
        ComboText.rectTransform.localScale = Vector3.one;
        comboTweener = ComboText.rectTransform.DOPunchScale(Vector3.one * 1.3f, 0.4f, 2);
    }

    int showedForPlayCount;
    float lastShowedTime;
    public void TryShowSponsoredVideo()
    {
        if (GameController.Instance.PlayCount < C.ads_show_min_playcount.IntValue && GameController.Instance.PlayTime < C.ads_show_min_seconds.FloatValue)
            return;
        if (showedForPlayCount == GameController.Instance.PlayCount)
            return;
        showedForPlayCount = GameController.Instance.PlayCount;
        if (!AdsSystem.Instance.enabled)
            return;
#if !UNITY_EDITOR
        if (!AdsSystem.Instance.IsIntersLoaded)
        {
            AdsSystem.Instance.LoadInters();
            return;
        }
#endif
        if (Time.time - lastShowedTime < 30)
        {
            return;
        }
        lastShowedTime = Time.time;
        UIController.Instance.SponsoredView.gameObject.SetActive(true);
        UniTask.Delay(3000).ContinueWith(() =>
        {
            //if (UnityEngine.Random.value > 0.5)
            //    AdsSystem.Instance.ShowReward(null);
            //else
            AdsSystem.Instance.ShowInters();
        });
        UniTask.Delay(2000).ContinueWith(() =>
        {
            UIController.Instance.SponsoredView.gameObject.SetActive(false);
        });
    }

    public void TryClaimX()
    {
        AdsSystem.Instance.ShowReward(
        (success) =>
        {
            if (success)
            {
                // earnedMoney = ScoreToMoney(GameController.Instance.Score);
                MarketController.Instance.Money += 2 * earnedMoney;

                IncreaseAmountText.text = "[X3]  +" + earnedMoney;
                OverScoreText.text = GameController.Instance.Score.ToString("N0");
                //StartCoroutine(LostScoreText.Counter(
                //    GameController.Instance.Score,
                //    0,
                //    3));
                StartCoroutine(EarnedMoneyText.Counter(
                    MarketController.Instance.Money - 1 * earnedMoney,
                    MarketController.Instance.Money,
                    2f, //Mathf.Clamp((float)earnedMoney / 10, 0f, 1f),
                    0.05f,
                    delay: 0.5f,
                    setFormat: (v) => "$ " + v.ToString("N0"),
                    everyStep: (i) => SoundController.Instance.Play(CounterSound, 0.3f)));

                lastShowedGameScore = GameController.Instance.PlayCount;

                ClaimX_Button.gameObject.SetActive(false);
            }
        });
    }

    int ScoreToMoney(int score) => score / 10;
}
