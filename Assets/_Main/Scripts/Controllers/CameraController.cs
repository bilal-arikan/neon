﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Arikan;

public class CameraController : SingletonBehaviour<CameraController>
{
    static Camera cameraMain;
    public static Camera CameraMain => cameraMain ?? (cameraMain = Camera.main);

    public Transform Target;
    public float LerpAmount = 0.8f;
    public Vector3 Offset;
    public Transform P1;
    public Transform P2;

    [SerializeField]
    Vector3 nextPose;
    Transform CamTransform;
    float maxReachedHeight;
    int CameraIndex;

    const float CamWidth = 5.765472f;//2f * 2.882736f;//
    const float OrthoMax = 7.000000f;//2f * 2.882736f;//
    const float OrthoMin = 4.500000f;//2f * 2.882736f;//

    [NonSerialized]
    public Action<Transform> FocusedTargetChanged;

    protected override void Awake()
    {
        //0.5625

        base.Awake();
        //CameraMain.orthographicSize = 5f / (9f/16f) * 2f;
        //CameraMain.aspect = 1080f / Screen.height;
        CamTransform = CameraMain.transform;
        // CameraMain.orthographicSize = Mathf.Clamp((CamWidth / 2) * ((float)Screen.height / (float)Screen.width), OrthoMin, OrthoMax);
        P1.transform.localPosition = P1.transform.localPosition.SetY(CameraMain.orthographicSize);
        P2.transform.localPosition = P2.transform.localPosition.SetY(-CameraMain.orthographicSize);

        SetEnabledPostProcessing(PlayerPrefs.GetInt("EnabledPostProcessing", 1) == 1);
    }


    public void Follow(Transform target) => Follow(target, false);
    public void Follow(Transform target, bool newOffset)
    {
        var oldTarget = Target;
        maxReachedHeight = CamTransform.position.y;

        Target = target;
        if (oldTarget != target)
            FocusedTargetChanged?.Invoke(target);
        if (newOffset && target)
            Offset = CamTransform.position - Target.position;
    }

    private void Update()
    {
        //CameraMain.aspect = 1080f / 0.5625f
        //int newIndex = (int)(Camera.main.transform.position.y / wallHeight);
        //if (newIndex != CameraIndex)
        //{
        //    transform.position = transform.position.SetY(wallHeight * newIndex);

        //    CameraIndex = newIndex;
        //}

        if (Target)
        {
            nextPose = new Vector3(CamTransform.position.x, Mathf.Clamp(Target.position.y + Offset.y, maxReachedHeight, float.MaxValue), CamTransform.position.z);
            //nextPose = new Vector3(
            //   true ? transform.position.x : Target.position.x + Offset.x,
            //   false ? transform.position.y : Target.position.y + Offset.y,
            //   true ? transform.position.z : Target.position.z + Offset.z
            //   );

            CamTransform.position = Vector3.Lerp(CamTransform.position, nextPose, LerpAmount * Time.deltaTime);
            maxReachedHeight = CamTransform.position.y;
        }
    }

    public bool IsEnabledPostProcessing() => UIController.Instance.PPL.enabled;
    public void SetEnabledPostProcessing(bool enabled)
    {
        UIController.Instance.PPL.enabled = enabled;
        UIController.Instance.PPV.enabled = enabled;
        PlayerPrefs.SetInt("EnabledPostProcessing", enabled ? 1 : 0);
        Debug.Log("PP Enabled:" + enabled);
    }
}
