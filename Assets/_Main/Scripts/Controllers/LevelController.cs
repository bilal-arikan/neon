﻿using Arikan;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class LevelController : SingletonBehaviour<LevelController>
{
    public LevelButton LevelButtonPrefab;
    public LevelData[] Levels;
    public Transform LevelsRoot;
    public LevelButton[] LevelButtons;

    Queue<Chunk> ActiveChunks = new Queue<Chunk>();
    Chunk LastSpawnedChunk;
    int LastSpawnedChunkIndex;
    int MinChunkCount = 3;
    public int CurrentLevel { get; private set; }
    public LevelData CurrentData;
    public bool FinishLineShowed;

    private IEnumerator Start()
    {
        ClearMap();
        LoadMap(-1);
        UpdateMap();

        while (true)
        {
            if (Ball.FollowingBall)
            {
                BackgroundColorManager.Instance.SetBG((int)Ball.FollowingBall.TravelDistance);
            }

            yield return new WaitForSeconds(BackgroundColorManager.Instance.changeDuration);
        }
    }

    void Update()
    {
        // Altta Kalan Chunkları silici
        if (ActiveChunks.Count > 0 && CameraController.CameraMain.transform.position.y - 10 > ActiveChunks.Peek().TopWorldPose.y)
        {
            var c = ActiveChunks.Dequeue();
            ChunkPool.Instance.RemoveToPool(c);
            UpdateMap();
        }
    }


    public void Complete(int index)
    {
        if (index < 0)
        {
            Debug.LogError("-1 could not be completed");
            return;
        }

        var data = Levels[index];
        Debug.Log("Level: " + index + " Completed");
        PlayerPrefs.SetInt("Level" + index, 1);

        LevelButtons.ForEach(l => l.OnEnable());
    }
    public bool IsCompleted(int index)
    {
        return PlayerPrefs.GetInt("Level" + index, 0) == 1;
    }

    public void LoadMap(int level)
    {
        ChunkPool.Instance.PoolReset();
        CurrentLevel = level;
        FinishLineShowed = false;
        if (level > -1)
            CurrentData = Levels[CurrentLevel];
    }
    public void LoadMapFull(int level)
    {
        ChunkPool.Instance.PoolReset();
        CurrentLevel = level;
        if (level > -1)
            CurrentData = Levels[CurrentLevel];

        for (int i = 0; i < CurrentData.Chunks.Length; i++)
        {
            var spawnPose = LastSpawnedChunk ? LastSpawnedChunk.TopWorldPose : GameController.Instance.SpawnPose;

            LastSpawnedChunkIndex++;
            LastSpawnedChunk = ChunkPool.Instance.Spawn(spawnPose, CurrentData.Chunks[LastSpawnedChunkIndex].Chunk);
            LastSpawnedChunk.SetMirror(CurrentData.Chunks[LastSpawnedChunkIndex].Flipped);
        }
        var spawnPose2 = LastSpawnedChunk ? LastSpawnedChunk.TopWorldPose : GameController.Instance.SpawnPose;

        LastSpawnedChunk = ChunkPool.Instance.Spawn(spawnPose2, -1);
        if (!FinishLineShowed)
        {
            UIController.Instance.ShowFinishLine(spawnPose2);
            FinishLineShowed = true;
        }
    }
    public void ClearMap()
    {
        LastSpawnedChunk = null;
        LastSpawnedChunkIndex = -1;
        while (ActiveChunks.Count > 0)
        {
            var c = ActiveChunks.Dequeue();
            ChunkPool.Instance.RemoveToPool(c);
        }
        CurrentData = null;
    }
    public void UpdateMap()
    {
        while (ActiveChunks.Count < MinChunkCount)
        {
            var spawnPose = LastSpawnedChunk ? LastSpawnedChunk.TopWorldPose : GameController.Instance.SpawnPose;
            if (CurrentLevel != -1)
            {
                if (LastSpawnedChunkIndex >= CurrentData.Chunks.Length - 1)
                {
                    LastSpawnedChunk = ChunkPool.Instance.Spawn(spawnPose, -1);
                    if (!FinishLineShowed)
                    {
                        UIController.Instance.ShowFinishLine(spawnPose);
                        FinishLineShowed = true;
                        return;
                    }
                }
                else
                {
                    LastSpawnedChunkIndex++;
                    LastSpawnedChunk = ChunkPool.Instance.Spawn(spawnPose, CurrentData.Chunks[LastSpawnedChunkIndex].Chunk);
                    LastSpawnedChunk.SetMirror(CurrentData.Chunks[LastSpawnedChunkIndex].Flipped);
                }
            }
            else
            {
                LastSpawnedChunk = ChunkPool.Instance.Spawn(spawnPose);
                LastSpawnedChunk.SetMirror(UnityEngine.Random.value > 0.5f);
            }

            ActiveChunks.Enqueue(LastSpawnedChunk);
        }

        //Debug.Log("Map Updated");
    }

#if UNITY_EDITOR
    [Button]
    void GetLevels()
    {
        //Levels = UnityEditor.AssetDatabase.LoadAllAssetRepresentationsAtPath("Assets/Neon/Resources/Levels/").ConvertAll(l => l as LevelData);
        Levels = Resources.LoadAll<LevelData>("Levels/");
        LevelButtons = LevelsRoot.GetComponentsInChildren<LevelButton>();
    }
    [Button]
    void GenerateLevelButtons()
    {
        //Levels = UnityEditor.AssetDatabase.LoadAllAssetRepresentationsAtPath("Assets/Neon/Resources/Levels/").ConvertAll(l => l as LevelData);
        Levels = Resources.LoadAll<LevelData>("Levels/");
        LevelButtons = LevelsRoot.GetComponentsInChildren<LevelButton>();
        LevelsRoot.gameObject.DestroyChildren();
        foreach (var lvl in Levels)
        {
            var btn = UnityEditor.PrefabUtility.InstantiatePrefab(LevelButtonPrefab, LevelsRoot);
        }
        LevelButtons = LevelsRoot.GetComponentsInChildren<LevelButton>();
        GetLevels();
    }
#endif
}
