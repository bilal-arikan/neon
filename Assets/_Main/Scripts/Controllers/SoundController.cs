﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arikan;
using UnityEngine;

public class SoundController : SingletonBehaviour<SoundController>
{
    public AudioSource MusicSource;
    public AudioSource[] Sources;
    public AudioClip WonClip;
    public AudioClip LostClip;
    int currentIndex = 0;


    public void Play(AudioClip clip) => Play(clip, 1f);
    public void Play(AudioClip clip, float volume = 1f)
    {
        if (!clip || !enabled)
            return;
        //Sources[currentIndex].Play(clip, AudioInterruptMode.PlayOverExisting);
        Sources[0].PlayOneShot(clip, volume);
        currentIndex = (currentIndex + 1) % Sources.Length;
    }

    public void StopAll()
    {
        Sources.ForEach(s => s.Stop());
    }

    private void OnEnable()
    {

    }
    private void OnDisable()
    {

    }
}
