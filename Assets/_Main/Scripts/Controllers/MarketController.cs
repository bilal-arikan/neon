﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arikan;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;

[Serializable]
public class MarketItem
{
    public string Id;
    public string Desc;
    public int Price;
    public string Ctgry;

    public MarketItem(string id, string desc, int price, string ctgry = "")
    {
        Id = id;
        Desc = desc;
        Price = price;
        Ctgry = ctgry;
    }
}

public class MarketController : SingletonBehaviour<MarketController>
{
    public MarketItemView[] Items;
    public GameObject ItemsRoot;
    public MarketItemView MIVPrefab;

    [NonSerialized]
    public static Action<string> OnItemSelected;

    static int IAPMoneyAmount => C.iap_gold_amount.IntValue;
    static int AdsMoneyAmount => C.ads_gold_amount.IntValue;
    static int LikeMoneyAmount => C.like_gold_amount.IntValue;
    static int ShrMoneyAmount => C.shr_gold_amount.IntValue;
    bool likeRewardGiven
    {
        get => PlayerPrefs.GetInt("LikeReward", 0) == 1;
        set => PlayerPrefs.SetInt("LikeReward", value ? 1 : 0);
    }
    bool shareRewardGiven
    {
        get => PlayerPrefs.GetInt("ShrReward", 0) == 1;
        set => PlayerPrefs.SetInt("ShrReward", value ? 1 : 0);
    }

    Coroutine moneyChange;

    public int Money
    {
        get => PlayerPrefs.GetInt("Money", 500);
        set
        {
            if (UIController.Instance.MoneyText.gameObject.activeInHierarchy)
            {
                if (moneyChange != null)
                    StopCoroutine(moneyChange);
                moneyChange = StartCoroutine(UIController.Instance.MoneyText.Counter(
                    Money,
                    value,
                    2, //Mathf.Clamp((float)Mathf.Abs(value - Money) / 10, 0, 2f),
                    stepDuration: 0.05f,
                    delay: 0.1f,
                    setFormat: (v) => "$ " + v.ToString("N0"),
                    everyStep: (i) => SoundController.Instance.Play(UIController.Instance.CounterSound, 0.3f)));
            }
            else
            {
                UIController.Instance.MoneyText.text = value.ToString("N0");
            }
            PlayerPrefs.SetInt("Money", value);
            MarketController.Instance.Items.ForEach(i => i.OnEnable());
        }
    }

    protected override void Awake()
    {
        void UpdateMarketUI()
        {
            UIController.Instance.IAPMoneyText.text = "+" + IAPMoneyAmount;
            UIController.Instance.AdsMoneyText.text = "+" + AdsMoneyAmount;
            UIController.Instance.LikeMoneyText.text = "+" + LikeMoneyAmount;
            UIController.Instance.ShrMoneyText.text = "+" + ShrMoneyAmount;
            UIController.Instance.LikeMoneyButton.gameObject.SetActive(!likeRewardGiven);
            UIController.Instance.ShrMoneyButton.gameObject.SetActive(!shareRewardGiven);
        }

        base.Awake();
        Money = Money;
        Items = ItemsRoot.GetComponentsInChildren<MarketItemView>(true);
        UpdateMarketUI();
        UIController.Instance.IAPMoneyButton.onClick.AddListener(() =>
        {
            IAPSystem.Instance.BuyProduct("money1");
            // Firebase.Analytics.FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLogin);
        });
        UIController.Instance.AdsMoneyButton.onClick.AddListener(() =>
        {
            AdsSystem.Instance.ShowReward((grantCB) => Money += AdsMoneyAmount);
        });
        UIController.Instance.LikeMoneyButton.onClick.AddListener(() =>
        {
            Application.OpenURL("market://details?id=com.arikan.neon");
            if (likeRewardGiven)
                return;
            likeRewardGiven = true;
            UniTask.Delay(4000).ContinueWith(() =>
            {
                Money += LikeMoneyAmount;
                UIController.Instance.LikeMoneyButton.gameObject.SetActive(!likeRewardGiven);
            });
        });
        UIController.Instance.ShrMoneyButton.onClick.AddListener(() =>
        {
            // NativeShare.ShareMultiple("https://play.google.com/store/apps/details?id=com.arikan.neon");
            // if (shareRewardGiven)
            //     return;
            // shareRewardGiven = true;
            // this.Invoke(() =>
            // {
            //     Money += ShrMoneyAmount;
            //     UIController.Instance.ShrMoneyButton.gameObject.SetActive(!shareRewardGiven);
            // }, 4);
        });
        E.ConfigsFetched += UpdateMarketUI;

        E.ItemPurchased += (id) =>
        {
            if (id == "noads")
            {
                PlayerPrefs.SetInt("NoAds", 1);
                AdsSystem.Instance.enabled = false;
            }
            else if (id == "money1")
            {
                MarketController.Instance.Money += IAPMoneyAmount;
            }
        };
        E.ItemPurchasingError += (id) =>
        {
        };
    }

    private void Start()
    {
        foreach (var item in Items)
            if (item.IsSelected)
                OnItemSelected?.Invoke(item.Info.Id);
    }

    public void OnBuy(MarketItemView item)
    {
        if (MarketController.Instance.Money < item.Info.Price)
        {
            Debug.LogError("Para Yetersizken Satın Alınmaya Çalışıldı !!!");
            return;
        }
        PlayerPrefs.SetInt(item.name, 1);
        MarketController.Instance.Money -= item.Info.Price;
        OnSelect(item);
    }
    public void OnSelect(MarketItemView item)
    {
        var sames = Items.Where(i => i.Info.Ctgry == item.Info.Ctgry);
        foreach (var i in sames)
            if (i != item && i.IsPurchased)
                OnDeselect(i);
        PlayerPrefs.SetInt(item.name, 2);
        OnItemSelected?.Invoke(item.name);
    }
    public void OnDeselect(MarketItemView item)
    {
        PlayerPrefs.SetInt(item.name, 1);
    }


    [Button]
    void AddMoney()
    {
        Money += 10000;
    }
}
