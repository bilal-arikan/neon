﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Arikan;
using Arikan.Pooling;
using Arikan.Tools;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.UI;

public class GameController : SingletonBehaviour<GameController>
{
    int hitScore;
    int altitudeScore => (int)(CameraController.CameraMain.transform.position.y - SpawnPose.y) * 10;
    int combo;
    public int Score => hitScore + altitudeScore;

    public int Combo
    {
        get => combo;
        set
        {
            if (!Application.isPlaying)
                return;
            combo = Mathf.Clamp(value, 1, 10);
            UIController.Instance.SetComboText(combo);
        }
    }


    public float StartDelay = 1.5f;
    public float ThrowDelay = 1.5f;
    float lastThrowedTime;
    public int[] ScoreLevels;
    [Header("Prefabs")]
    public Ball BallPrefab;
    public Ball[] BallPrefabs;
    public TextMesh EarnedPointPrefab;
    [Header("Refs")]
    public Collider2D[] DeathZones;
    public NewStage NewStageInstance;
    public FinishLine FinishLineInstance;

    Vector3 clickPose;
    bool isDown;
    public bool IsPlaying { get; set; }
    float startedTime;
    [NonSerialized]
    public Vector3 SpawnPose;
    public int PlayCount { get; private set; }
    public float PlayTime { get; private set; }
    public int CurrentLevel { get; private set; }

    private Tweener throwSliderTweener;

    [NonSerialized]
    public int Stage;
    int oldStage;


    // Start is called before the first frame update
    protected override void Awake()
    {
        base.Awake();

        Application.targetFrameRate = 120;
        SpawnPose = CameraController.CameraMain.transform.position.SetZ(0);

        MarketController.OnItemSelected += (id) =>
        {
            // Debug.Log("Item Selected:" + id);
            switch (id)
            {
                case "ball1": BallPrefab = BallPrefabs.Find(b => b.name == "Ball1"); break;
                case "ball2": BallPrefab = BallPrefabs.Find(b => b.name == "Ball2"); break;
                case "ball3": BallPrefab = BallPrefabs.Find(b => b.name == "Ball3"); break;
                case "ball4": BallPrefab = BallPrefabs.Find(b => b.name == "Ball4"); break;

                case "delay100": ThrowDelay = 2f; break;
                case "delay75": ThrowDelay = 1.5f; break;
                case "delay50": ThrowDelay = 1f; break;
                case "delay25": ThrowDelay = 0.5f; break;

                //case "power10": Ball.ThrowPower = 10; break;
                //case "power15": Ball.ThrowPower = 10.8f; break;
                //case "power20": Ball.ThrowPower = 11.5f; break;
                //case "power25": Ball.ThrowPower = 12.5f; break;
                case "power10": Ball.ThrowPower = 10; break;
                case "power15": Ball.ThrowPower = 11f; break;
                case "power20": Ball.ThrowPower = 13f; break;
                case "power25": Ball.ThrowPower = 15f; break;

                default:
                    Debug.LogError("Tanımlanmamış Market Item !!!", this);
                    break;
            }
        };
        //E.ConfigsFetched += () =>
        //{
        //    var newScoreLevels = Utils.FromJson<int[]>(C.difficulty_steps.StringValue);
        //    if (newScoreLevels.Length > 0)
        //    {
        //        ScoreLevels = newScoreLevels;
        //        Debug.Log("New ScoreLevels Parsed:\n" + C.difficulty_steps.StringValue);
        //    }
        //};

        // ca-app-pub-5742130512939823~4119581989
        Arikan.Modules.AdmobModule.Instance.Init();
    }

    private void Start()
    {
        // // Initialize Firebase
        // Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        // {
        //     var dependencyStatus = task.Result;
        //     if (dependencyStatus == Firebase.DependencyStatus.Available)
        //     {
        //         // Create and hold a reference to your FirebaseApp,
        //         // where app is a Firebase.FirebaseApp property of your application class.
        //         // Crashlytics will use the DefaultInstance, as well;
        //         // this ensures that Crashlytics is initialized.
        //         Firebase.FirebaseApp app = Firebase.FirebaseApp.DefaultInstance;
        //         Firebase.Crashlytics.Crashlytics.SetUserId(Environment.UserName);
        //         // Set a flag here for indicating that your project is ready to use Firebase.
        //         Debug.Log("Firebase CheckAndFixDependenciesAsync Done");
        //     }
        //     else
        //     {
        //         UnityEngine.Debug.LogError(System.String.Format(
        //           "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
        //         // Firebase Unity SDK is not safe to use here.
        //     }
        // });
    }

    public void PlayFree() => StartCoroutine(PlayCoRo(-1));
    public void Play(int level) => StartCoroutine(PlayCoRo(level));
    public void PlayNext() => StartCoroutine(PlayCoRo(LevelController.Instance.CurrentLevel + 1));
    public void Restart() => StartCoroutine(PlayCoRo(LevelController.Instance.CurrentLevel));
    IEnumerator PlayCoRo(int level)
    {
        if (IsPlaying)
        {
            throw new UnityException("Already Playing !!!");
        }
        if (level > 0 && !LevelController.Instance.IsCompleted(level - 1))
        {
            throw new UnityException("Level is not Unlocked !!!");
        }

        CurrentLevel = level;
        SoundController.Instance.StopAll();
        hitScore = 0;
        Combo = 1;
        lastThrowedTime = 0;
        UIController.Instance.SliderThrow.value = 1;
        isDown = false;
        Time.timeScale = 1f;

        UIController.Instance.ScoreText.text = "0";
        UIController.Instance.ChangeColorTo(CColor.black, 0.1f);
        UIController.Instance.ShowGameView();
        UIController.Instance.ShowStageChanged(0);
        UIController.Instance.HideFinishLine();
        var directStart = UIController.Instance.ShowLevelInfo(CurrentLevel);

        oldStage = -1;
        Stage = 0;
        LevelController.Instance.ClearMap();
        LevelController.Instance.LoadMap(level);
        LevelController.Instance.UpdateMap();
        ResetableTransform.ResetAll();

        PlayCount++;
        //if (PlayCount > 2 && PlayCount % 3 == 0)
        //{
        //    UIController.Instance.ShowSponsoredVideo();
        //    yield return new WaitForSecondsRealtime(5);
        //}

        if (directStart)
            IsPlaying = true;
        yield return new WaitForSeconds(StartDelay);

        var ball = Ball.Spawn(SpawnPose);
        CameraController.Instance.Follow(ball.transform, false);

        startedTime = Time.unscaledTime;
        if (level < 0)
            Debug.Log("--- Game Started ---");
        else
            Debug.Log("--- Level Started --- : " + level);
    }



    void Update()
    {

        if (!IsPlaying || Ball.Instances.Count == 0)
            return;

        if (LevelController.Instance.CurrentLevel == -1)
        {
            Stage = CalculateStage();
            if (Stage != oldStage)
            {
                UIController.Instance.ShowStageChanged(Stage);
                LevelController.Instance.UpdateMap();
                // Debug.Log("Stage:" + Stage);
            }
            oldStage = Stage;
        }


        UIController.Instance.ScoreText.text = Score.ToString("N0");
        UIController.Instance.SliderThrow.value = Mathf.Clamp01((Time.time - lastThrowedTime) / ThrowDelay);
        if (UIController.Instance.SliderThrow.value == 1)
        {
            UIController.Instance.SliderThrowFillImage.color = UIController.Instance.SliderThrowAvailable;
            throwSliderTweener?.Complete();
        }

        if (!isDown && Input.GetKey(KeyCode.Mouse0))
        {
            if (Time.time - lastThrowedTime < ThrowDelay)
            {
                UIController.Instance.SliderThrowFillImage.color = UIController.Instance.SliderThrowNotAvailable;
                throwSliderTweener?.Complete();
                throwSliderTweener = UIController.Instance.SliderThrowFillImage.rectTransform.DOPunchScale(new Vector3(1, 2, 1), 0.5f, 2);
            }
            else
            {
                clickPose = Input.mousePosition;
                Time.timeScale = 0.1f;
                isDown = true;
                // Ball.Instances.ForEach(b => b.DirectionLine.enabled = true);

                UIController.Instance.ChangeColorTo(CColor.olivedrab, 0.3f);
            }
        }
        else if (isDown && Input.GetKeyUp(KeyCode.Mouse0))
        {
            Combo = 1;
            Vector3 direction = (clickPose - Input.mousePosition).normalized;

            Time.timeScale = 1f;
            isDown = false;
            UIController.Instance.ChangeColorTo(CColor.transparent, 0.3f);
            // Ball.Instances.ForEach(b => b.DirectionLine.enabled = false);
            Ball.Instances.ForEach(b => b.Throw(direction));
            lastThrowedTime = Time.time;
        }
        if (isDown)
        {
            //Ball.Instance.SetDirection((clickPose - Input.mousePosition).normalized);
            var touchWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Ball.Instances.ForEach(b => b.SetDirection((clickPose - Input.mousePosition).normalized, touchWorldPosition));
        }
    }

    public void OnHit(OtherObject oo)
    {
        SpawnParticles(oo.transform.position, oo.Rend.color);
        if (oo.ScoreValue == 0)
            return;
        var ep = ObjectPooler.Instance.Instantiate(EarnedPointPrefab.name, oo.transform.position, Quaternion.identity).GetComponent<TextMesh>();

        //var ep = Instantiate(EarnedPointPrefab, oo.transform.position, Quaternion.identity);
        //Destroy(ep.gameObject, 3);
        UniTask.Delay(3000).ContinueWith(() => ep.gameObject.RemoveToPool());
        ep.text = (oo.ScoreValue * combo).ToString();
        ep.color = UIController.Instance.ComboColors[Mathf.Clamp(combo - 1, 0, UIController.Instance.ComboColors.Count - 1)];

        hitScore += oo.ScoreValue * combo;
        Combo++;
    }

    public void TryGameOver() => StartCoroutine(TryGameOverCoRo());
    IEnumerator TryGameOverCoRo()
    {
        yield return new WaitForEndOfFrame();
        if (Ball.Instances.Count > 0)
            yield break;

        // Debug.Log("TryGameOver");
        if (!IsPlaying)
            yield break;
        IsPlaying = false;

        Debug.Log("--- Game Over ---");
        OnGameEnded();
        SoundController.Instance.Play(SoundController.Instance.LostClip);

        UIController.Instance.ChangeColorTo(CColor.red, 0.1f);
        yield return new WaitForSecondsRealtime(1);
        UIController.Instance.ShowResult(false);
    }

    public void TryWin() => StartCoroutine(TryWinCoRo());
    IEnumerator TryWinCoRo()
    {
        // Debug.Log("TryWin");
        if (!IsPlaying)
            yield break;
        IsPlaying = false;

        Debug.Log("--- WON ---");
        OnGameEnded();
        SoundController.Instance.Play(SoundController.Instance.WonClip);

        LevelController.Instance.Complete(CurrentLevel);
        UIController.Instance.ChangeColorTo(CColor.green, 0.1f);
        yield return new WaitForSecondsRealtime(1);
        UIController.Instance.ShowResult(true);
        yield return new WaitForSecondsRealtime(1);
        Ball.Instances.ToArray().ForEach(b => b.Death());
    }

    public void StopPlaying()
    {
        IsPlaying = false;
        Ball.Instances.ToArray().ForEach(b => b.gameObject.Destroy());
    }

    void OnGameEnded()
    {
        PlayTime = Time.unscaledTime - startedTime;
        // Debug.Log("Seconds: " + PlayTime);

        // if (UIController.Instance.VibrationEnabled)
        //     Handheld.Vibrate();

        SetPaused(false);
        IsPlaying = false;
    }

    float pausedTime;
    public void SetPaused(bool paused)
    {
        isDown = false;
        IsPlaying = !paused;

        if (paused)
            pausedTime = Time.unscaledTime;
        else
            startedTime -= (Time.unscaledTime - pausedTime);

        Time.timeScale = paused ? 0 : 1;
        if (paused)
            UIController.Instance.ShowPausedView();
        else
            UIController.Instance.ShowGameView();
        //Ball.Instance.Stop();
        Ball.Instances.ForEach(b => b.Stop());
    }

    public static void SpawnParticles(Vector3 pose, Color c, int numToSpawn = 20, int lengthMultiplier = 40, float speedOffset = 0.01f)
    {
        var initialScale = new Vector2(2f, 1f);
        float duration = 120f;

        for (int i = 0; i < numToSpawn; i++)
        {
            float speed = (18f * (1f - 1 / UnityEngine.Random.Range(1f, 10f))) * speedOffset;

            var state = new PE2D.ParticleBuilder()
            {
                velocity = PE2D.StaticExtensions.Random.RandomVector2(speed, speed),
                wrapAroundType = PE2D.WrapAroundType.None,
                lengthMultiplier = lengthMultiplier,
                velocityDampModifier = 0.94f,
                removeWhenAlphaReachesThreshold = true
            };

            PE2D.ParticleFactory.instance.CreateParticle(pose, c, duration, initialScale, state);
        }
    }


    int CalculateStage()
    {
        int score = Score;
        int length = ScoreLevels.Length;
        for (int i = 0; i < length; i++)
            if (score < ScoreLevels[i])
                return i;
        return ScoreLevels.Length;
    }
}
