﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Arikan;


public class ChunkPool : SingletonBehaviour<ChunkPool>
{
    public bool FirstChunkDebug;
    public bool RandomChunkDebug;
    public Chunk EmptyChunkPrefab;
    public Chunk[] ChunkPrefabs;

    Tuple<int, int>[] ChunkIndexAndLevels;
    Queue<int> lastSpawneds = new Queue<int>();
    Stack<Chunk>[] Pools = new Stack<Chunk>[0];

    // Olası en yüksek Level
    Chunk[] SpawnableChunkPrefabs;
    int lastIndex;

    [Header("Levels")]
    [SerializeField]
    public LevelData[] LevelInfos = new LevelData[0];

    protected override void Awake()
    {
        base.Awake();

        Pools = new Stack<Chunk>[ChunkPrefabs.Length];
        for (int i = 0; i < ChunkPrefabs.Length; i++)
        {
            Pools[i] = new Stack<Chunk>();
        }

        ChunkIndexAndLevels = new Tuple<int, int>[ChunkPrefabs.Length];
        for (int i = 0; i < ChunkPrefabs.Length; i++)
        {
            ChunkIndexAndLevels[i] = new Tuple<int, int>(i, ChunkPrefabs[i].MinSpawnableLevel);
        }
    }

    public Chunk Spawn(Vector3 worldPose)
    {
#if UNITY_EDITOR
        if (FirstChunkDebug)
            return Spawn(worldPose, 0);
        if (RandomChunkDebug)
            return Spawn(worldPose, UnityEngine.Random.Range(0, ChunkPrefabs.Length));
#endif
        int level = GameController.Instance.Stage;
        var possibleIndexes = ChunkIndexAndLevels.Where(il => il.Item2 <= level && !lastSpawneds.Contains(il.Item1)).Select(il => il.Item1).ToArray();
        return Spawn(worldPose, possibleIndexes.GetRandom());
    }
    public Chunk Spawn(Vector3 worldPose, Chunk c) => Spawn(worldPose, ChunkPrefabs.IndexOf(c));
    public Chunk Spawn(Vector3 worldPose, int i)
    {
        if (i == -1)
        {
            var empty = Instantiate(EmptyChunkPrefab, worldPose, Quaternion.identity);
            return empty;
        }
        lastSpawneds.Enqueue(i);
        if (lastSpawneds.Count > 5)
            lastSpawneds.Dequeue();
        if (Pools[i].Count > 0)
        {
            var c = Pools[i].Pop();
            c.gameObject.SetActive(true);
            c.transform.position = worldPose;
            return c;
        }
        else
        {
            var c = Instantiate(ChunkPrefabs[i], worldPose, Quaternion.identity);
            c.name = ChunkPrefabs[i].name;
            return c;
        }
    }

    public void PoolReset()
    {
        lastSpawneds.Clear();
    }

    public void RemoveToPool(Chunk c)
    {
        c.gameObject.SetActive(false);
        int index = Array.FindIndex(ChunkPrefabs, cp => cp.name == c.name);
        if (index > 0)
            Pools[index].Push(c);
    }
}
