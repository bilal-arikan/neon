﻿using Arikan.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Arikan.Triggers
{
    public class Collider2DTrigger : MonoBehaviour
    {
        public LayerMask OtherMask;
        public bool AvailableForCollisions = true;
        public bool AvailableForTriggers = true;

        [Space]
        public UnityEvent OnEnter;

        public UnityEvent OnExit;


        protected void OnCollisionEnter2D(Collision2D other)
        {
            if (AvailableForCollisions)
                if (OtherMask == (OtherMask | (1 << other.gameObject.layer)))
                    OnEnter.Invoke();
        }

        protected void OnTriggerEnter2D(Collider2D other)
        {
            if (AvailableForTriggers)
                if (OtherMask == (OtherMask | (1 << other.gameObject.layer)))
                    OnEnter.Invoke();
        }

        protected void OnCollisionExit2D(Collision2D other)
        {
            if (AvailableForCollisions)
                if (OtherMask == (OtherMask | (1 << other.gameObject.layer)))
                    OnExit.Invoke();
        }

        protected void OnTriggerExit2D(Collider2D other)
        {
            if (AvailableForTriggers)
                if (OtherMask == (OtherMask | (1 << other.gameObject.layer)))
                    OnExit.Invoke();
        }
    }
}
