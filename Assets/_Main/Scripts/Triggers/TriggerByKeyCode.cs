﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class TriggerByKeyCode : MonoBehaviour
{
    public KeyCode Key;

    public UnityEvent OnDown;
    public UnityEvent OnUp;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(Key))
            OnDown.Invoke();
        if (Input.GetKeyUp(Key))
            OnUp.Invoke();
    }
}
