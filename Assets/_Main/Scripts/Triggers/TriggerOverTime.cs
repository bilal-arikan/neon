﻿using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

public class TriggerOverTime : TriggerBehaviour
{

    public float Seconds = 3;
    public bool RealTime = true;
    [ReadOnly]
    public bool Canceled;

    UniTask coro;

    public override void Trigger()
    {
        coro = TriggerCo();
    }

    public void Cancel()
    {
        Canceled = true;
    }

    // Use this for initialization
    async UniTask TriggerCo()
    {
        Canceled = false;

        await UniTask.Delay((int)Seconds * 1000, RealTime);
        if (!Canceled)
            OnTriggered.Invoke();
    }
}
