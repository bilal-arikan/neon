﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerOnAwake : MonoBehaviour, ITriggerable {

    public UnityEvent OnTriggered;

    public void Trigger() => Awake();

    // Use this for initialization
    void Awake() {
        OnTriggered.Invoke();
	}
}
