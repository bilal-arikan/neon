﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Arikan.Triggers
{
    public class TriggerOncePlayTime : MonoBehaviour, ITriggerable
    {
        static List<string> Triggereds = new List<string>();

        public string Key = "Once";

        public UnityEvent OnTriggered;

        public void Trigger()
        {
            if (!Triggereds.Contains(Key))
            {
                OnTriggered.Invoke();
                Triggereds.Add(Key);
            }
        }

    }
}
