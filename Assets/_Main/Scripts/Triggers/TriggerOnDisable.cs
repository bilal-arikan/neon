﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerOnDisable : MonoBehaviour, ITriggerable {

    public UnityEvent OnTriggered;

    public void Trigger() => OnDisable();

    void OnDisable()
    {
        OnTriggered.Invoke();
    }
}
