﻿using Arikan.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Arikan.Components
{
    public class ColliderTrigger : MonoBehaviour
    {
        public LayerMask OtherMask;
        public bool AvailableForCollisions = true;
        public bool AvailableForTriggers = true;

        [Space]
        public UnityEvent OnEnter;

        public UnityEvent OnExit;


        protected void OnCollisionEnter(Collision other)
        {
            if (AvailableForCollisions)
                if (OtherMask == (OtherMask | (1 << other.gameObject.layer)))
                {
                    OnEnter.Invoke();
                }
        }

        protected void OnTriggerEnter(Collider other)
        {
            if (AvailableForTriggers)
                if (OtherMask == (OtherMask | (1 << other.gameObject.layer)))
                {
                    OnEnter.Invoke();
                }
        }

        protected void OnCollisionExit(Collision other)
        {
            if (AvailableForCollisions)
                if (OtherMask == (OtherMask | (1 << other.gameObject.layer)))
                    OnExit.Invoke();
        }

        protected void OnTriggerExit(Collider other)
        {
            if (AvailableForTriggers)
                if (OtherMask == (OtherMask | (1 << other.gameObject.layer)))
                    OnExit.Invoke();
        }
    }
}
