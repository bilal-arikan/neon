﻿using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum TriggerMethod
{
    Manually,
    OnEnable,
    OnStart,
    OnDisable,
    OnDestroy
}

public interface ITriggerable
{
    void Trigger();
}

public class TriggerBehaviour : SerializedMonoBehaviour, ITriggerable
{
    public TriggerMethod Method;
    public UnityEvent OnTriggered;

    [Button]
    public virtual void Trigger() => OnTriggered.Invoke();

    protected virtual void OnEnable()
    {
        if (Method == TriggerMethod.OnEnable)
            Trigger();
    }

    protected virtual void Start()
    {
        if (Method == TriggerMethod.OnStart)
            Trigger();
    }

    protected virtual void OnDisable()
    {
        if (Method == TriggerMethod.OnDisable)
            Trigger();
    }

    protected virtual void OnDestroy()
    {
        if (Method == TriggerMethod.OnDestroy)
            Trigger();
    }
}
