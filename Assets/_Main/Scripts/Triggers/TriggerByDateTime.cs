﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

public class TriggerByDateTime : TriggerBehaviour
{
    public int BeforeYear = DateTime.UtcNow.Year;
    public int BeforeMonth = DateTime.UtcNow.Month;
    public int BeforeDay = DateTime.UtcNow.Day;
    public int BeforeHour = DateTime.UtcNow.Hour;
    public int BeforeMinute = DateTime.UtcNow.Minute;
    public int BeforeSecond = DateTime.UtcNow.Second;
    [Space]
    public int AfterYear = DateTime.UtcNow.Year;
    public int AfterMonth = DateTime.UtcNow.Month;
    public int AfterDay = DateTime.UtcNow.Day;
    public int AfterHour = DateTime.UtcNow.Hour;
    public int AfterMinute = DateTime.UtcNow.Minute;
    public int AfterSecond = DateTime.UtcNow.Second;


    public override void Trigger()
    {
        DateTime before = new DateTime(BeforeYear, BeforeMonth, BeforeDay, BeforeHour, BeforeMinute, BeforeSecond, DateTimeKind.Utc).ToUniversalTime();
        DateTime after = new DateTime(AfterYear, AfterMonth, AfterDay, AfterHour, AfterMinute, AfterSecond, DateTimeKind.Utc).ToUniversalTime();

        Debug.Log("TriggerByDateTime " + DateTime.UtcNow.ToString());
        Debug.Log(before.ToString() + "\n" + after.ToString());

        if (after > DateTime.UtcNow && DateTime.UtcNow > before)
            OnTriggered.Invoke();
    }
}
