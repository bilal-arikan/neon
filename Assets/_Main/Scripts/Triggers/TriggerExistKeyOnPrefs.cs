﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class TriggerExistKeyOnPrefs : TriggerBehaviour
{
    public string Key;

    public override void Trigger()
    {
        if (PlayerPrefs.HasKey(Key))
            OnTriggered.Invoke();
    }
}
