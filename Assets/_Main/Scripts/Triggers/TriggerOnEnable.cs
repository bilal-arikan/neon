﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerOnEnable : MonoBehaviour, ITriggerable {

    public UnityEvent OnTriggered;

    public void Trigger() => OnEnable();

    // Use this for initialization
    void OnEnable () {
        OnTriggered.Invoke();
	}
}
