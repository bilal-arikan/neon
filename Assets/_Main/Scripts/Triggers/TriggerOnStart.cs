﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerOnStart : MonoBehaviour, ITriggerable {

    public UnityEvent OnTriggered;

    public void Trigger() => Start();

    // Use this for initialization
    void Start () {
        OnTriggered.Invoke();
	}
}
