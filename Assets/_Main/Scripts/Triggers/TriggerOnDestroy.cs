﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerOnDestroy : MonoBehaviour, ITriggerable {

    public UnityEvent OnTriggered;

    public void Trigger() => OnDestroy();

    void OnDestroy()
    {
        OnTriggered.Invoke();
    }
}
