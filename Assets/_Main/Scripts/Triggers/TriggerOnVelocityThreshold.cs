﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Bilal.Garage
{
    [ExecuteInEditMode]
    public class TriggerOnSpeedChanged : MonoBehaviour
    {
        public Rigidbody Body;
        public float Threshold;
        public UnityEvent SpeedDecreased;
        public UnityEvent SpeedIncreased;

        float oldVelocity = float.MaxValue;
        float currentVelocity;

        private void OnEnable()
        {
            if (!Body)
            {
                Body = transform.GetComponentsInParent<Rigidbody>().FirstOrDefault();
            }
        }


        void FixedUpdate()
        {
            currentVelocity = Body.velocity.magnitude;

            if (oldVelocity < Threshold && currentVelocity > Threshold)
            {
                SpeedIncreased.Invoke();
            }
            else if(oldVelocity > Threshold && currentVelocity < Threshold)
            {
                SpeedDecreased.Invoke();
            }

            oldVelocity = currentVelocity;
        }
    }
}
