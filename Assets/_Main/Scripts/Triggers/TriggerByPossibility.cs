﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

public class TriggerByPossibility : TriggerBehaviour
{
    [Range(0f, 1f)]
    public float TriggerPossibility = 1f;

    public override void Trigger()
    {
        if (UnityEngine.Random.value < TriggerPossibility)
            OnTriggered.Invoke();
    }
}
