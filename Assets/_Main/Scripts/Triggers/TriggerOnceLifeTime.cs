﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

public class TriggerOnceLifetime : TriggerBehaviour
{
    public string Key;

    [ReadOnly]
    public bool TriggeredBefore;

    public override void Trigger()
    {
        if (string.IsNullOrEmpty(Key))
            Debug.LogError("TriggerOnceLifetime Key Empty !!!");

        TriggeredBefore = PlayerPrefs.GetInt("OnceLifetime" + Key, 0) == 1;
        if (!TriggeredBefore)
        {
            OnTriggered.Invoke();
            SaveKey();
        }
    }

    public void SaveKey()
    {
        PlayerPrefs.SetInt("OnceLifetime" + Key, 1);
    }
}