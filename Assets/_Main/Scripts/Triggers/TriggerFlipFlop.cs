﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Bilal.Components.Common
{
    public class TriggerFlipFlop : MonoBehaviour, ITriggerable
    {
        public bool IsFlip = true;

        public UnityEvent OnFlip;

        public UnityEvent OnFlop;

        void Awake()
        {
            OnFlip.AddListener(OnTrigger);
            OnFlop.AddListener(OnTrigger);
        }

        public void Trigger()
        {
            if (IsFlip)
                OnFlip.Invoke();
            else
                OnFlop.Invoke();
        }

        void OnTrigger()
        {
            IsFlip = !IsFlip;
        }
    }
}
