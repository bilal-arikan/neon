﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Arikan;
using UnityEngine;
using UnityEngine.UI;

public class ImageFadeTo : MonoBehaviour
{

    Image image;
    Coroutine coro;
    float startTime;

    private void Start()
    {
        image = GetComponent<Image>();
    }


    public void FadeTo(Color c, float duration)
    {
        if (coro != null)
            StopCoroutine(coro);
        coro = StartCoroutine(SetPanelColorCoRo(c, duration));
    }
    IEnumerator SetPanelColorCoRo(Color c, float duration)
    {
        startTime = Time.unscaledTime;
        do
        {
            float alpha = (Time.unscaledTime - startTime) / duration;
            image.color = Color.Lerp(CColor.transparent, c, alpha);
            //Debug.Log(alpha + "/" + startTime + "/" + Time.unscaledTime);

            yield return null;
        } while (Time.unscaledTime < startTime + duration);
        image.color = c;
        //Debug.Log("Fade Completed");
    }
}
