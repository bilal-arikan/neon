﻿using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;

public class Ball : MultipleBehaviour<Ball>
{
    public static float ThrowPower = 10;
    public static Ball FollowingBall;

    public LineRenderer DirectionLine;
    public LineRenderer FingerSwipeLine;
    public TrailRenderer TrailLine;
    public Rigidbody2D Rigid;
    public SpriteRenderer Rend;
    public SpriteRenderer ShieldRend;
    public AudioClip SpawnSound;
    public AudioClip DeathSound;

    const string DefaultLayer = "Ball";
    const string NoCollideLayer = "NoCollide";
    public bool IsShilded;
    public float TravelDistance => transform.position.y - startPosition.y;

    private Vector3 startPosition;

    // Start is called before the first frame update
    void Start()
    {
        GameController.SpawnParticles(transform.position, Rend.color, 100, 60);
        foreach (var b in Ball.Instances)
        {
            if (FollowingBall == null)
            {
                FollowingBall = b;
                continue;
            }
            else if (FollowingBall.transform.position.y < b.transform.position.y)
            {
                FollowingBall = b;
            }
        }
        CameraController.Instance.Follow(FollowingBall.transform, false);
        //SoundController.Instance.Play(SpawnSound);
        PE2D.CustomParticle.UpdateEffectorList();
        startPosition = transform.position;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        GameController.SpawnParticles(transform.position, Color.red, 100, 60);
    }

    //private void OnValidate()
    //{
    //    TrailLine.widthCurve = new AnimationCurve(
    //        new Keyframe(0, 0),
    //        new Keyframe(1, 1)
    //        );
    //}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            SetShielded(3);
        if (Input.GetKeyDown(KeyCode.Alpha2))
            SetNoCollide(6);
        if (Input.GetKeyDown(KeyCode.Alpha3))
            gameObject.SetLayer(DefaultLayer);
        if (Input.GetKeyDown(KeyCode.Alpha4))
            Stop();

        if (transform.position.x > 3.2f || transform.position.x < -3.2f || transform.position.y < CameraController.CameraMain.transform.position.y - 50)
            Death();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("other"))
        {
            var other = collision.gameObject.GetComponent<OtherObject>();
            GameController.Instance.OnHit(other);
            other?.OnHit(this);
        }
        else if (collision.gameObject.tag.Equals("DeathZone"))
        {
            Death();
        }
        else if (collision.gameObject.tag.Equals("Finish"))
        {
            GameController.Instance.TryWin();
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("Hit");
        if (collision.gameObject.tag.Equals("other"))
        {
            var other = collision.gameObject.GetComponent<OtherObject>();
            GameController.Instance.OnHit(other);
            other?.OnHit(this);
        }
        else if (collision.gameObject.tag.Equals("DeathZone"))
        {
            Death();
        }
    }

    public static Ball Spawn(Vector3 worldPose)
    {
        var ball = Instantiate(GameController.Instance.BallPrefab, worldPose.SetZ(-1), Quaternion.identity);
        if (Ball.Instances.Count > 1)
            ball.Rigid.gravityScale = 1f;
        return ball;
    }

    [Button]
    public void Stop()
    {
        Rigid.velocity = Vector2.zero;
        Rigid.gravityScale = 0;
    }
    public void SetDirection(Vector3 direction, Vector3 touchWorldPosition)
    {
        if (!DirectionLine.enabled)
        {
            DirectionLine.enabled = true;
        }
        DirectionLine.SetPosition(1, direction * 10);
        // if (!FingerSwipeLine.enabled)
        // {
        //     FingerSwipeLine.enabled = true;
        // }
        // FingerSwipeLine.SetPosition(0, transform.position);
        // FingerSwipeLine.SetPosition(1, touchWorldPosition);
    }
    public void Throw(Vector3 direction)
    {
        DirectionLine.enabled = false;
        // FingerSwipeLine.enabled = false;
        Rigid.velocity = direction * ThrowPower;
        Rigid.gravityScale = 1;
        gameObject.SetLayer(DefaultLayer);
    }

    public async void SetNoCollide(float minSpeed)
    {
        //Debug.Log("No Collide " + minSpeed);
        gameObject.SetLayer(NoCollideLayer);
        await UniTask.WaitUntil(() => Rigid.velocity.y < minSpeed);
        gameObject.SetLayer(DefaultLayer);
    }

    Coroutine shieldCoRo;
    public void SetShielded(float seconds)
    {
        Debug.Log("Shielded " + seconds);
        IsShilded = true;
        ShieldRend.gameObject.SetActive(true);
        //PE2D.CustomParticle.UpdateEffectorList();

        IEnumerator SetShieldFalse()
        {
            yield return new WaitForSecondsRealtime(seconds);
            IsShilded = false;
            ShieldRend.gameObject.SetActive(false);
            //PE2D.CustomParticle.UpdateEffectorList();
        }

        if (shieldCoRo != null)
            StopCoroutine(shieldCoRo);
        shieldCoRo = StartCoroutine(SetShieldFalse());
    }

    public void Death()
    {
        // SoundController.Instance.Play(DeathSound);
        Destroy(gameObject);
        GameController.Instance.TryGameOver();
    }

    void OnDestroy()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying)
            return;
#endif
        PE2D.CustomParticle.UpdateEffectorList();
        foreach (var b in Ball.Instances)
        {
            if (FollowingBall == null)
            {
                FollowingBall = b;
                continue;
            }
            else if (FollowingBall.transform.position.y < b.transform.position.y)
            {
                FollowingBall = b;
            }
        }
        CameraController.Instance.Follow(FollowingBall?.transform, false);
    }
}
