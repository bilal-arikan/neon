﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Arikan;

public class ParallaxWall : SerializedMonoBehaviour
{
    ObjectBounds Left;
    ObjectBounds Right;

    [ShowInInspector]
    int CameraIndex;

    float wallHeight;

    void Start()
    {
        Left = transform.GetChild(0).GetComponent<ObjectBounds>();
        Right = transform.GetChild(1).GetComponent<ObjectBounds>();
        wallHeight = Left.Bounds.size.y;
        //CameraController.Instance.CamIndexChanged += (newIndex) => transform.position = transform.position.SetY(wallHeight * newIndex);
    }
}
