﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arikan;
using Cysharp.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;

[Serializable]
public class HighScores
{
    public List<Score> Scores = new List<Score>();
}
[Serializable]
public struct Score
{
    public string name;
    public int score;
}

public class HighScoreController : SingletonBehaviour<HighScoreController>
{
    public HighScores HighScores;


    private void Start()
    {
        var temp = JsonConvert.DeserializeObject<HighScores>(PlayerPrefs.GetString("HighScores", "{}")).Scores;
        HighScores.Scores = temp.OrderByDescending((h1) => h1.score).ToList();
        if (HighScores.Scores.Count > 10)
            HighScores.Scores = HighScores.Scores.GetRange(0, 10);

        List<ScoreView> views = new List<ScoreView>();
        for (int i = 0; i < HighScores.Scores.Count; i++)
        {
            var sv = Instantiate(UIController.Instance.ScoreViewPrefab, UIController.Instance.ScoresParent.transform);
            views.Add(sv);
            sv.Init(HighScores.Scores[i]);
        }
        UniTask.Delay(0).ContinueWith(() =>
        {
            views.ForEach(scr => scr.Order.text = (scr.transform.GetSiblingIndex() + 1) + ".");
            views.ForEach(scr => scr.SetCurrent(false));
        });
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            AddScore(new Score() { name = "bll", score = UnityEngine.Random.Range(5, 1564) });
        }
    }

    public void AddScore(Score s)
    {
        HighScores.Scores.Add(s);
        HighScores.Scores = HighScores.Scores.OrderByDescending((h1) => h1.score).ToList();
        if (HighScores.Scores.Count > 10)
            HighScores.Scores = HighScores.Scores.GetRange(0, 10);

        int length = UIController.Instance.ScoresParent.transform.childCount;
        for (int i = 0; i < length; i++)
        {
            Destroy(UIController.Instance.ScoresParent.transform.GetChild(i).gameObject);
        }

        List<ScoreView> views = new List<ScoreView>();
        for (int i = 0; i < HighScores.Scores.Count; i++)
        {
            var sv = Instantiate(UIController.Instance.ScoreViewPrefab, UIController.Instance.ScoresParent.transform);
            views.Add(sv);
            sv.Init(HighScores.Scores[i]);
        }

        UniTask.Delay(0).ContinueWith(() =>
        {
            views.ForEach(scr => scr.Order.text = (scr.transform.GetSiblingIndex() + 1) + ".");
            views.ForEach(scr => scr.SetCurrent(scr.Value.score == s.score));
        });

        SaveScores();
    }

    public int GetOrder(int score)
    {
        int order = HighScores.Scores.Count;
        for (int i = HighScores.Scores.Count - 1; i > -1; i--)
        {
            if (HighScores.Scores[i].score < score)
                order--;
        }
        return order;
    }

    void SaveScores()
    {
        PlayerPrefs.SetString("HighScores", JsonConvert.SerializeObject(HighScores));
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        SaveScores();
    }
}
