﻿using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GeneratedLevel : SerializedMonoBehaviour
{
    public LevelData.ChunkPlacement[] Chunks = new LevelData.ChunkPlacement[0];


    [Button]
    public void CreateResource(string name)
    {
#if UnityEditor

        var zero = UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Neon/Resources/Levels/000", typeof(LevelData));

        UnityEditor.AssetDatabase.CreateAsset(zero, "Assets/Neon/Resources/Levels/" + name);
        Debug.Log("Created " + name, zero);
#endif        
    }
}
