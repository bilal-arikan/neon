﻿using Arikan;
using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class LevelGenerator : SingletonBehaviour<LevelGenerator>
{
    public Transform SpawnPose;

    [Header("Parameters")]
    public int levelAmount = 10;
    public int chunkAmount = 5;
    public int zerosAmount = 2;
    public Chunk[] Spawneds = new Chunk[0];
    public GeneratedLevel[] Generateds = new GeneratedLevel[0];

#if UNITY_EDITOR
    [Button]
    void Generate()
    {
        for (int i = 0; i < Generateds.Length; i++)
            if (Generateds[i])
                Generateds[i].gameObject.Destroy();

        var zeros = ChunkPool.Instance.ChunkPrefabs.FindAll(c => c.MinSpawnableLevel == 0);
        Spawneds = new Chunk[chunkAmount];
        Generateds = new GeneratedLevel[levelAmount];
        for (int j = 0; j < levelAmount; j++)
        {
            var Chunks = new LevelData.ChunkPlacement[chunkAmount];

            Generateds[j] = new GameObject(j.ToString()).AddComponent<GeneratedLevel>();
            Generateds[j].transform.parent = transform;
            Generateds[j].transform.position = SpawnPose.position.AddX(j * 10);

            Vector3 currentPose = SpawnPose.position.AddX(j * 10);
            for (int i = 0; i < chunkAmount; i++)
            {
                Chunk c = null;
                if (i < zerosAmount)
                    c = zeros.GetRandom();
                else
                    c = ChunkPool.Instance.ChunkPrefabs.GetRandom();

                var s = UnityEditor.PrefabUtility.InstantiatePrefab(c, Generateds[j].transform) as Chunk;
                s.transform.SetPositionAndRotation(currentPose, Quaternion.identity);
                bool flip = UnityEngine.Random.value > 0.5f;
                Chunks[i] = new LevelData.ChunkPlacement()
                {
                    Chunk = c,
                    Flipped = flip,
                };
                Spawneds[i] = s;
                currentPose += new Vector3(0, s.Size.y, 0);
            }
            Generateds[j].Chunks = Chunks;
            Debug.Log("Level Generated: " + chunkAmount, this);
        }
    }

    public GeneratedLevel Spawn(LevelData levelData)
    {
        for (int i = 0; i < Generateds.Length; i++)
            if (Generateds[i])
                Generateds[i].gameObject.Destroy();

        var Chunks = new LevelData.ChunkPlacement[chunkAmount];
        var g = new GameObject(levelData.name).AddComponent<GeneratedLevel>();
        g.transform.parent = transform;
        Generateds[0] = g;
        g.transform.position = Vector3.zero;

        Vector3 currentPose = Vector3.zero;
        for (int i = 0; i < levelData.Chunks.Length; i++)
        {
            bool flip = levelData.Chunks[i].Flipped;

            var s = UnityEditor.PrefabUtility.InstantiatePrefab(levelData.Chunks[i].Chunk, g.transform) as Chunk;
            s.SetMirror(levelData.Chunks[i].Flipped);

            s.transform.SetPositionAndRotation(currentPose, Quaternion.identity);

            Chunks[i] = new LevelData.ChunkPlacement()
            {
                Chunk = levelData.Chunks[i].Chunk,
                Flipped = flip,
            };
            Spawneds[i] = s;
            currentPose += new Vector3(0, s.Size.y, 0);
        }
        g.Chunks = Chunks;
        Debug.Log("Level Generated: " + chunkAmount, this);
        return g;
    }
#endif
}
