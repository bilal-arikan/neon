﻿using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using Arikan;

public class ChunkGenerator : SingletonBehaviour<ChunkGenerator>
{
    [Serializable]
    public struct OtherParameters
    {
        public bool Available;
        public int MinAmount;
        public int MaxAmount;
        public bool RandomRotation;
    }

    Vector3 wallLeftPose = new Vector3(-3, 5, 0);
    Vector3 wallRghtPose = new Vector3(3, 5, 0);
    public Transform SpawnRoot;
    public SpriteRenderer WallPrefab;

    [Header("Settings")]
    public Vector3 Size = new Vector3(5, 20, 0);
    public Dictionary<OtherObject, OtherParameters> AvailableOthers = new Dictionary<OtherObject, OtherParameters>();

    [Button]
    public void Generate()
    {
        float mult = Size.y / 10;

        GameObject go = new GameObject("Chunk_G_");
        go.transform.position = SpawnRoot.position;
        var c = go.AddComponent<Chunk>();
        c.Size = Size;
        var wl = Instantiate(WallPrefab, go.transform);
        wl.transform.localPosition = wallLeftPose.SetY(Size.y / 2);
        wl.size = new Vector2(wl.size.x, wl.size.y * mult);
        wl.GetComponent<BoxCollider2D>().size = new Vector2(wl.GetComponent<BoxCollider2D>().size.x, wl.GetComponent<BoxCollider2D>().size.y * mult);

        var wr = Instantiate(WallPrefab, go.transform);
        wr.transform.localPosition = wallRghtPose.SetY(Size.y / 2);
        wr.size = new Vector2(wr.size.x, wr.size.y * mult);
        wr.flipX = true;
        wr.GetComponent<BoxCollider2D>().size = new Vector2(wr.GetComponent<BoxCollider2D>().size.x, wr.GetComponent<BoxCollider2D>().size.y * mult);


        //Instantiate()
    }
}
