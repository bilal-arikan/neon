// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("qwhYHJxRbEc9gLFkSmWx0RhyJN5tW2RtaD52eGpqlG9uW2hqapRbdmNAbWpubmxpan11Ax8fGxhRREQcZm1iQe0j7Zxmampubmto6Wpqazdbem1oPm9heGEqGxsHDksiBQhFWhsHDks5BAQfSygqW3V8ZltdW19ZHwINAggKHw5LCRJLCgUSSxsKGR9LBA1LHwMOSx8DDgVLChsbBwIICuQY6gutcDBiRPnZky8jmwtT9X6eQe0j7ZxmampubmtbCVpgW2JtaD4cHEUKGxsHDkUIBAZEChsbBw4ICn1bf21oPm9oeGYqGxsHDks5BAQfonIZnjZlvhQ08JlOaNE+5CY2ZpoFD0sIBAUPAh8CBAUYSwQNSx4YDtWfGPCFuQ9koBIkX7PJVZITlACj639AuwIs/x1ilZ8A5kUrzZwsJhREW+qobWNAbWpubmxpaVvq3XHq2AzkY99LnKDHR0sEG91Ualvn3CikFCrD85K6oQ33TwB6u8jQj3BBqHRWTQxL4VgBnGbppLWAyESSOAEwDxsHDksoDhkfAg0CCAofAgQFSyoeRSvNnCwmFGM1W3RtaD52SG9zW30HDksiBQhFWk1bT21oPm9geHYqG01bT21oPm9geHYqGxsHDksoDhkfGQoIHwIIDksYHwofDgYOBR8YRVvDtxVJXqFOvrJkvQC/yU9IepzKxxJLChgYHgYOGEsKCAgOGx8KBQgObWg+dmVvfW9/QLsCLP8dYpWfAOZLKCpb6WpJW2ZtYkHtI+2cZmpqasDIGvksOD6qxEQq2JOQiBumjcgndPqwdSw7gG6GNRLvRoBdyTwnPocisx30WH8Oyhz/okZpaGprasjpakdLCA4ZHwINAggKHw5LGwQHAggST4mAutwbtGQuikyhmgYThozefHxeWVpfW1hdMXxmWF5bWVtSWVpfW7JdFKrsPrLM8tJZKZCzvhr1Fco5D15IfiB+MnbY/5yd9/WkO9GqMzveUcafZGVr+WDaSn1FH75XZrAJfQINAggKHwIEBUsqHh8DBBkCHxJab214aT44Wnhbem1oPm9heGEqGxtjNVvpanptaD52S2/pamNb6WpvW/71EWfPLOAwv31cWKCvZCalfwK6HwMEGQIfElp9W39taD5vaHhmKhtd8idGE9yG5/C3mBzwmR25HFskqi4VdCcAO/0q4q8fCWB76CrsWOHqZPZWmEAiQ3GjlaXe0mWyNXe9oFbccNb4KU95Qaxkdt0m9zUIoyDrfFvpb9Bb6WjIy2hpamlpamlbZm1ibIcWUujgOEu4U6/a1PEkYQCUQJcJBw5LGB8KBQ8KGQ9LHw4ZBhhLClhdMVsJWmBbYm1oPm9teGk+OFp4EVvpah1bZW1oPnZkamqUb29oaWraWzOHMW9Z5wPY5Ha1DhiUDDUO1+lqa21iQe0j7ZwID25qW+qZW0Ftbmto6Wpka1vpamFp6Wpqa4/6wmJ07ujucPJWLFyZwvAr5Ue/2vt5s+By4rWSIAeebMBJW2mDc1WTO2K4SwoFD0sIDhkfAg0CCAofAgQFSxsyzG5iF3wrPXp1H7jc4EhQLMi+BDkOBwIKBQgOSwQFSx8DAhhLCA4ZO8HhvrGPl7tibFzbHh5K");
        private static int[] order = new int[] { 57,29,35,47,9,6,39,41,55,29,36,12,19,43,50,32,37,21,21,56,29,31,47,36,46,54,40,30,43,46,32,32,44,53,41,58,46,46,56,58,53,44,58,48,54,47,59,57,51,58,55,51,53,54,55,58,59,57,58,59,60 };
        private static int key = 107;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
