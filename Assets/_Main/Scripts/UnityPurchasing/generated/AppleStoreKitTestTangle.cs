// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleStoreKitTestTangle
    {
        private static byte[] data = System.Convert.FromBase64String("VxIyEywlIHYnKS8rcFdMUUZoSlcSMhMsJSB2JygvK3BXTFFGaEpXEqiP1ryTKhuATVjoBXMpOcKq8lFVEyklKwglIiYmJCAgEy4lKgmla6XjZAtoyHniCCp1D4aJKiBYERL/y6Zba+3j5jEySSwvjQwm6UxZR1wDMaD1pFtOup59kCnDu+3VyeCtTkohdnASNBM2JSoJpWul1C4iKiI1KykvK3BXTFFGaEpXEjITLCUgdico8FUz0dNKz+5BulLaQ6mnaxvnnqjsW49+mR18cwh/Cq/+4fQJNPwBl2KGXSqumR4e7lN0+87M9vlU67AnLytwV0xRRmhKVxIoEyolIHYnJTCvwlL4aWbQDjq1IHWihWfNVlR4cCLcJycgISGnEzUlIHY+BiIi3CcvaLCYKduCAh3rQvoH6308Vh35sPgToSBXE6Ehf4MgISIhISIiEy4lKnBXTFFGaEpXEz00LhETExcTEhIUJyAvK3BXTFFGaEpXEjITLCUgdicPVsVvkjHw3sA8fS5em3v0g1/HXOeKKJDsQSzYl5/U+Kd9C8hkLtLOIxOhIikhoSIiI/hcsx8PUCz4tAUoEyolIHYnJTAhdnASNBM2JSoJpRI5sqOanMYv6B+YLWdBBdknCpVqPjAiItwnJhMgIiLcEy0lIHY+LCKe4/U+Y710mhuzVFVx8YtoU02bRD/MKqtbRu2i0a5RxCQ5TQUx1BGSCaVrpdQuIiIoJiMTfBIyEywlIHbETMM4xS5L7gZN4FUr1mkojVlXH0CItLkI4An785HC9nkuGjhMRAuKFhEQFnk0LhcTExARFBIUFhEQFnnsRx9Oo3oQr+kVJQkyOK5q7SvcRdQuIiIoJiMgoSIiI5Ejwx/Sy8pB4djFfDHiISAiIyKAGBMaEywlIHZrpdQuIioiNStwV0xRRmhKVxOhIgETLiUqCaVrpdQuIiIiJiMgoSIsLpgbs0gl598uxrZz7rLrmQbXe+pDVRRxz9yPOn4vNgypLNaq6/QIPigab+hsTeOQfJS2mZqLfmlXiMxMyJRjoh6kHlNy8ud+rob/XEl7Of1EBpqITxBGjvvhuVtrLMrv4gQu5RN8EjITLCUgdicgLytwV0xRRmhKutiQqXa8zD4Lm3wJ8lC8FmQUmGX0x1uLjItOhOL5Sw5F7+wKKHMPwD3PCLImhB5s");
        private static int[] order = new int[] { 9,10,19,30,37,38,41,38,30,34,25,21,37,24,29,15,34,41,19,39,39,29,29,34,28,32,29,32,41,40,38,36,36,36,37,41,37,43,39,42,41,42,43,43,44 };
        private static int key = 35;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
