// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("HtnTWvHwlwuRrsIBOTlLovqLpUuljaeJ30mHeTQsS/CZjK4JTosiWjIv1U7zCmKH8k2S78L6Uxk4dGs6MwDaXmElv1ISwtdB84VWmDsZ0FmPWUFxAi2GWPEJ+jpjFMuWRoHzfPFEZKc6vqjH9mF49+30vF4WZ8if4lDT8OLf1Nv4VJpUJd/T09PX0tG44ke5+YIyeiWZ9D1VvJrj6w0xjFetzaLOCQ93GZf5aIiUi9+d0z5xVgE7tDtWDPMPjzJ5B6KRrfomvTKm+EC67FF6krOqM1j/2BKi/QObs1DT3dLiUNPY0FDT09JFekQu6mhdQEtnD0x3wOhqMhN9OQdtzZpGhkqlwXr1iQZwPPeF1eAi3CvBXcO6VmluyF2/2Ciy1dDR09LT");
        private static int[] order = new int[] { 4,10,3,11,8,6,8,12,11,9,12,12,13,13,14 };
        private static int key = 210;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
