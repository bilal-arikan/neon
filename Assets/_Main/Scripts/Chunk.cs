﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chunk : SerializedMonoBehaviour
{
    public bool IsSpecial = true;
    public bool CanBeSymetric = false;
    public Vector3 Size = new Vector3(5,10);
    public OtherObject[] SubObjects;

    public Vector3 TopWorldPose => transform.position.AddY(Size.y);
    public int MinSpawnableLevel = 0;


    void OnValidate()
    {
        SubObjects = GetComponentsInChildren<OtherObject>();
    }

    private void OnEnable()
    {
        //if (CanBeSymetric && UnityEngine.Random.value > 0.5f)
        //{
        //    transform.localScale = transform.localScale.SetX(-transform.localScale.x);
        //}

        foreach (var so in SubObjects)
        {
            so.gameObject.SetActive(true);
        }
    }

    private void OnDisable()
    {
        SetMirror(false);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = CColor.yellow;
        Gizmos.DrawWireCube(transform.position + new Vector3(0, Size.y / 2, 0), Size);
    }

    public void SetMirror(bool isMirrored)
    {
        if(isMirrored)
            transform.localScale = transform.localScale.SetX(-Mathf.Abs(transform.localScale.x));
        else
            transform.localScale = transform.localScale.SetX( Mathf.Abs(transform.localScale.x));
    }
}
