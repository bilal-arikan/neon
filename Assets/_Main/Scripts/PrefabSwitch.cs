using UnityEngine;  // UnityEngine gives us general access.
#if UNITY_EDITOR
using UnityEditor;  // UnityEditor gives us editor-specific access.
#endif

/// <summary>Performs manual iteration to swap out one game object for another.</summary>
public class PrefabSwitch : MonoBehaviour
{
    /// <summary>The new object to instantiate in place of the old object.</summary>
    public GameObject newPrefab;
    public bool RandomYRotation = false;
    public Vector3 SpawnOffset;
    public bool onlySearchChildren = true;
    /// <summary>The old objects, intended to be swapped out for iterations of 
    /// the new object.</summary>
    public GameObject[] oldGameObjects;
    /// <summary>The string tag to use when replacing objects by tag.</summary>
    public string searchByTag;
    public string searchByName;
    public GameObject searchByRef;

#if UNITY_EDITOR
    /// <summary>Swaps all the game objects in oldGameObjects for 
    /// a new newPrefab.</summary>
    public void SwapAllByArray()
    {
        // Store a boolean to detect if we intend to swap this game object.
        bool swappingSelf = false;

        // For each game object in the oldGameObjects array, 
        for (int i = 0; i < oldGameObjects.Length; i++)
        {
            // If the current game object is this game object, 
            if (oldGameObjects[i] == gameObject)
            {
                // Enable the flag to swap this game object at the end, so we
                // do not destroy it before the script an complete its task.
                swappingSelf = true;
            }
            else
            {
                // Else, we are not dealing with the game object local to this 
                // script, so we can swap the prefabs, immediately. 
                oldGameObjects[i] = SwapPrefabs(oldGameObjects[i]);
            }
        }

        // If we have flagged the local game object to be swapped, 
        if (swappingSelf)
        {
            // Swap the local game object.
            SwapPrefabs(gameObject);
        }

        searchByName = newPrefab.name;
        searchByTag = newPrefab.tag;
    }

    /// <summary>Swaps all the game objects that use the tag <code>searchByTag</code>.
    /// If empty, we will use the tag of the local game object.</summary>
    public void FindAllByTag()
    {
        // If searchByTag is null, 
        if (searchByTag == "")
        {
            // Set searchByTag to the tag of the local game object.
            searchByTag = gameObject.tag;
        }

        // Find all the game objects using the tag searchByTag, 
        // store them in our array, and proceed to swapping them.
        if (onlySearchChildren)
        {
            oldGameObjects = gameObject.GetComponentsInChildren<Transform>().FindAll(o => o.tag == searchByTag).ConvertAll(o => o.gameObject);
        }
        else
        {
            oldGameObjects = GameObject.FindGameObjectsWithTag(searchByTag);
        }
    }

    /// <summary>Swaps all the game objects that use the tag <code>searchByTag</code>.
    /// If empty, we will use the tag of the local game object.</summary>
    public void FindAllByName()
    {
        // Find all the game objects using the tag searchByTag, 
        // store them in our array, and proceed to swapping them.
        if (onlySearchChildren)
        {

            oldGameObjects = gameObject.GetComponentsInChildren<Transform>().FindAll(o => o.name == searchByName).ConvertAll(o => o.gameObject);
        }
        else
        {
            oldGameObjects = GameObject.FindObjectsOfType<Transform>().FindAll(o => o.name == searchByName).ConvertAll(o => o.gameObject);
        }
    }
    public void FindAllByReference()
    {
        // Find all the game objects using the tag searchByRef, 
        // store them in our array, and proceed to swapping them.
        if (onlySearchChildren)
        {
            oldGameObjects = gameObject.GetComponentsInChildren<Transform>().FindAll(o => PrefabUtility.GetCorrespondingObjectFromOriginalSource(o.gameObject) == searchByRef).ConvertAll(o => o.gameObject);
        }
        else
        {
            oldGameObjects = GameObject.FindObjectsOfType<Transform>().FindAll(o => PrefabUtility.GetCorrespondingObjectFromOriginalSource(o.gameObject) == searchByRef).ConvertAll(o => o.gameObject);
        }
    }

    /// <summary>Swaps the desired oldGameObject for a newPrefab.</summary>
    /// <param name="oldGameObject">The old game object.</param>
    GameObject SwapPrefabs(GameObject oldGameObject)
    {
        // Determine the rotation and position values of the old game object.
        // Replace rotation with Quaternion.identity if you do not wish to keep rotation.
        Quaternion rotation = oldGameObject.transform.rotation;
        Vector3 position = oldGameObject.transform.position;

        // Instantiate the new game object at the old game objects position and rotation.
        // GameObject newGameObject = Instantiate(newPrefab, position + SpawnOffset, RandomYRotation ? rotation.WithRandomY() : rotation);
        GameObject newGameObject = PrefabUtility.InstantiatePrefab(newPrefab) as GameObject;
        newGameObject.transform.position = position + SpawnOffset;
        newGameObject.transform.rotation = RandomYRotation ? rotation.WithRandomY() : rotation;
        newGameObject.name = newGameObject.name.Replace("(Clone)", string.Empty);
        // If the old game object has a valid parent transform,
        // (You can remove this entire if statement if you do not wish to ensure your
        // new game object does not keep the parent of the old game object.
        if (oldGameObject.transform.parent != null)
        {
            // Set the new game object parent as the old game objects parent.
            newGameObject.transform.SetParent(oldGameObject.transform.parent);
        }

        // Destroy the old game object, immediately, so it takes effect in the editor.
        DestroyImmediate(oldGameObject);
        return newGameObject;
    }
#endif
}

#if UNITY_EDITOR
/// <summary>Custom Editor for our PrefabSwitch script, to allow us to perform actions
/// from the editor.</summary>
[CustomEditor(typeof(PrefabSwitch))]
public class PrefabSwitchEditor : Editor
{
    /// <summary>Calls on drawing the GUI for the inspector.</summary>
    public override void OnInspectorGUI()
    {
        // Draw the default inspector.
        DrawDefaultInspector();

        // Grab a reference to the target script, so we can identify it as a 
        // PrefabSwitch, instead of a simple Object.
        PrefabSwitch prefabSwitch = (PrefabSwitch)target;


        // Create a Button for "Swap By Tag",
        if(GUILayout.Button("Find By Tag"))
        {
            // if it is clicked, call the SwapAllByTag method from prefabSwitch.
            prefabSwitch.FindAllByTag();
        }

        // Create a Button for "Swap By Tag",
        if (GUILayout.Button("Find By name"))
        {
            // if it is clicked, call the SwapAllByTag method from prefabSwitch.
            prefabSwitch.FindAllByName();
        }

        // Create a Button for "Swap By Tag",
        if (GUILayout.Button("Find By Ref"))
        {
            // if it is clicked, call the SwapAllByTag method from prefabSwitch.
            prefabSwitch.FindAllByReference();
        }

        // Create a Button for "Swap By Array", 
        if (GUILayout.Button("Swap All By Array"))
        {
            // if it is clicked, call the SwapAllByArray method from prefabSwitch.
            prefabSwitch.SwapAllByArray();
        }


    }
}
#endif