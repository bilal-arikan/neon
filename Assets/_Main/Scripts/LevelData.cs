﻿using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "000", menuName = "LevelData")]
public class LevelData : SerializedScriptableObject
{
    [Serializable]
    public struct ChunkPlacement
    {
        public Chunk Chunk;
        public bool Flipped;
    }
    [Header(" ---  Level  --- ")]
    public int MoneyReward = 100;
    public ChunkPlacement[] Chunks = new ChunkPlacement[0];

#if UNITY_EDITOR
    [Button]
    void Preview()
    {
        LevelGenerator.Instance.Spawn(this);
    }
#endif
}
